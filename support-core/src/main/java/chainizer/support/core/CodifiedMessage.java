package chainizer.support.core;

/**
 * A formatted message.
 */
public interface CodifiedMessage {

    /**
     * The code of the message.
     *
     * @return the code
     */
    int code();

    /**
     * Format the message.
     *
     * @param params the params
     * @return the formatted message
     */
    String format(Object... params);

    /**
     * The name of the message
     *
     * @return the name
     */
    String name();

}
