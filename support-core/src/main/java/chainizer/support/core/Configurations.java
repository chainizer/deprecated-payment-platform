package chainizer.support.core;

import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.SystemConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.sync.ReadWriteSynchronizer;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.*;

public final class Configurations {

    private static final org.apache.commons.configuration2.builder.fluent.Configurations configurations = new org.apache.commons.configuration2.builder.fluent.Configurations();

    public static Configuration local(URL... confFile) {
        return combine(create("local"), confFile);
    }

    public static Configuration ci(URL... confFile) {
        return combine(create("ci"), confFile);
    }

    public static Configuration combine(Configuration appConfiguration, URL... confFiles) throws TechnicalException {
        String environment = appConfiguration.getString("environment");

        List<Configuration> configurationList = new ArrayList<>();
        configurationList.add(appConfiguration);

        for (URL confFile : confFiles) {
            try {
                Configuration configuration = configurations.properties(confFile);
                if (configuration != null) {
                    configurationList.add(configuration.subset("%" + environment));
                    configurationList.add(configuration);
                }
            } catch (ConfigurationException e) {
                throw new TechnicalException(e);
            }
        }

        return new CompositeConfiguration(configurationList);
    }

    public static Configuration create(String environment, URL... confUrls) throws TechnicalException {
        String prefix = null;
        if (isNotBlank(environment)) {
            prefix = "%" + environment;
        }

        try {
            List<Configuration> confList = new ArrayList<>();
            for (URL confUrl : confUrls) {
                Configuration configuration = configurations.properties(confUrl);
                if (configuration != null) {
                    confList.add(configuration);
                }
            }

            List<Configuration> filteredConfList = new ArrayList<>();
            if (isNoneBlank(prefix)) {
                for (Configuration extPropConf : confList) {
                    filteredConfList.add(extPropConf.subset(prefix));
                }
            }

            Configuration baseConf = new BaseConfiguration();
            baseConf.setProperty("environment", isBlank(environment) ? "" : environment);

            CompositeConfiguration configuration = new CompositeConfiguration(
                    Arrays.asList(new SystemConfiguration(), baseConf)
            );
            configuration.setSynchronizer(new ReadWriteSynchronizer());

            for (Configuration filteredProConf : filteredConfList) {
                configuration.addConfiguration(filteredProConf);
            }

            for (Configuration proConf : confList) {
                configuration.addConfiguration(proConf);
            }

            return configuration;
        } catch (ConfigurationException e) {
            throw new TechnicalException(e);
        }
    }

}
