package chainizer.support.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ShutdownHandlers {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShutdownHandlers.class);

    private static ShutdownHandlers INSTANCE;

    private static AtomicBoolean registered = new AtomicBoolean(false);

    private final List<Runnable> handlers = new ArrayList<>();

    private ShutdownHandlers() {
    }

    public static ShutdownHandlers get() {
        if (INSTANCE == null) {
            INSTANCE = new ShutdownHandlers();
        }
        return INSTANCE;
    }

    public ShutdownHandlers add(Runnable... handlers) {
        this.handlers.addAll(Arrays.asList(handlers));
        return this;
    }

    public void execute() {
        for (Runnable task : this.handlers) {
            try {
                LOGGER.info("execute task {}", task);
                task.run();
            } catch (Exception e) {
                LOGGER.warn("a task failed", e);
            }
        }
    }

    public ShutdownHandlers registerHook() {
        LOGGER.info("register");
        if (!registered.get()) {
            registered.set(true);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                LOGGER.info("JVM stopping");
                execute();
                LOGGER.info("JVM stopped");
            }));
        }
        return this;
    }

}
