package chainizer.support.core;

/**
 * An exception based on a code and an underlying message.
 */
public class CodifiedException extends Exception {

    private final CodifiedMessage codifiedMessage;

    public CodifiedException(CodifiedMessage codifiedMessage, Object... params) {
        super(codifiedMessage.name() + " - " + codifiedMessage.format(params));
        this.codifiedMessage = codifiedMessage;
    }

    public CodifiedException(Throwable cause, CodifiedMessage codifiedMessage, Object... params) {
        super(codifiedMessage.name() + " - " + codifiedMessage.format(params), cause);
        this.codifiedMessage = codifiedMessage;
    }

    public CodifiedMessage getCodifiedMessage() {
        return codifiedMessage;
    }

}
