package chainizer.support.core;

import java.util.function.Function;
import java.util.function.Supplier;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Utilities.
 */
public class Utils {

    public static String either(String input, String def) {
        return isBlank(input) ? def : input;
    }

    public static <T> T either(T input, Supplier<T> def) {
        return input == null ? def.get() : input;
    }

    public static <T, R> R convert(T input, R defaultValue, Function<T, R> function) {
        return input == null ? defaultValue : function.apply(input);
    }

}
