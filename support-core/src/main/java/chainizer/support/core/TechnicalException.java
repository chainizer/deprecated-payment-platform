package chainizer.support.core;

public class TechnicalException extends RuntimeException {

    public TechnicalException(CodifiedMessage codifiedMessage, Object... params) {
        super(codifiedMessage.name() + " - " + codifiedMessage.format(params));
    }

    public TechnicalException(Throwable cause) {
        super(cause);
    }

    public TechnicalException(Throwable cause, CodifiedMessage codifiedMessage, Object... params) {
        super(codifiedMessage.name() + " - " + codifiedMessage.format(params), cause);
    }

}
