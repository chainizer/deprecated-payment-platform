package chainizer.support.core;

public enum TechnicalErrorMsg implements CodifiedMessage {
    INPUT_NOT_VALID(400, "The input is not valid: [%s]"),
    UNEXPECTED_EXCEPTION(500, "An unexpected exception occurred.");

    private final int code;

    private final String template;

    TechnicalErrorMsg(int code, String template) {
        this.template = template;
        this.code = code;
    }

    @Override
    public String format(Object... params) {
        return String.format(template, params);
    }

    @Override
    public int code() {
        return code;
    }

}

