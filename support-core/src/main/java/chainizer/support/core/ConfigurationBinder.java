package chainizer.support.core;

import org.apache.commons.configuration2.Configuration;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class ConfigurationBinder extends AbstractBinder {

    private final Configuration configuration;

    public ConfigurationBinder(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    protected void configure() {
        bind(configuration).to(Configuration.class);
    }

}
