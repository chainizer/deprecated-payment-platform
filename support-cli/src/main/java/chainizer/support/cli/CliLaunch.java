package chainizer.support.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ServiceLoader;

public class CliLaunch {

    private static final Logger LOGGER = LoggerFactory.getLogger(CliLaunch.class);

    public static void main(String[] args) {
        JCommander jc = new JCommander();

        ServiceLoader<Command> commands = ServiceLoader.load(Command.class);
        for (Command command : commands) {
            jc.addCommand(command);
        }

        try {
            jc.parse(args);
        } catch (ParameterException e) {
            LOGGER.error("unable to parse parameter", e.getMessage());
            LOGGER.info("args: {}", args);
            jc.usage();
            System.exit(1);
        }

        if (jc.getCommands().containsKey(jc.getParsedCommand())) {
            JCommander jCommander = jc.getCommands().get(jc.getParsedCommand());
            Runnable command = (Runnable) jCommander.getObjects().get(0);
            LOGGER.info("execute {}", command);
            try {
                command.run();
            } catch (Exception e) {
                LOGGER.error("something went wrong", e);
                System.exit(3);
            }
        } else {
            jc.usage();
            System.exit(2);
        }
    }

}
