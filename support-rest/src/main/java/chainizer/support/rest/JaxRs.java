package chainizer.support.rest;

import chainizer.support.core.Configurations;
import io.netty.channel.Channel;
import org.apache.commons.configuration2.Configuration;
import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class JaxRs {

    private static final Logger LOGGER = LoggerFactory.getLogger(JaxRs.class);

    private final List<URL> configurations = new ArrayList<>();

    private final List<Object> objectsToRegister = new ArrayList<>();

    private final List<Class<?>> classesToRegister = new ArrayList<>();

    private String environment = System.getProperty("environment");

    private String uri = "http://0.0.0.0:9000/";

    private boolean health = false;

    private Function<Configuration, ResourceConfig> resourceConfigFactory;

    public static JaxRs configure() {
        return new JaxRs();
    }

    public JaxRs environment(String environment) {
        this.environment = environment;
        return this;
    }

    public JaxRs uri(String uri) {
        this.uri = uri;
        return this;
    }

    public JaxRs health(boolean health) {
        this.health = health;
        return this;
    }

    public JaxRs resourceConfigFactory(Function<Configuration, ResourceConfig> resourceConfigFactory) {
        this.resourceConfigFactory = resourceConfigFactory;
        return this;
    }

    public JaxRs configuration(URL... url) {
        configurations.addAll(Arrays.asList(url));
        return this;
    }

    public JaxRs register(Object object) {
        objectsToRegister.add(object);
        return this;
    }

    public JaxRs register(Class<?> clazz) {
        classesToRegister.add(clazz);
        return this;
    }

    public Channel serve() {
        LOGGER.info("starting");

        Configuration configuration = Configurations.create(
                environment,
                configurations.toArray(new URL[0])
        );

        URI baseUri = UriBuilder.fromUri(uri).build();

        ResourceConfig config;
        if (resourceConfigFactory != null) {
            config = resourceConfigFactory.apply(configuration);
        } else {
            config = new RestResourceConfig(configuration);
        }

        if (health) {
            config.registerClasses(DummyHealthzResource.class);
        }

        objectsToRegister.forEach(config::register);
        classesToRegister.forEach(config::registerClasses);

        Channel server = NettyHttpContainerProvider.createServer(baseUri, config, false);

        LOGGER.info("started");

        return server;
    }
}
