package chainizer.support.rest;

import chainizer.support.core.ConfigurationBinder;
import org.apache.commons.configuration2.Configuration;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.inject.Singleton;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.MessageBodyWriter;
import java.util.logging.Level;

public class RestResourceConfig extends ResourceConfig {

    public RestResourceConfig(Configuration configuration) {
        property(LoggingFeature.LOGGING_FEATURE_VERBOSITY_CLIENT, LoggingFeature.Verbosity.PAYLOAD_ANY);
        property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL, Level.FINE.getName());
        register(LoggingFeature.class);

        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);

        register(JacksonFeature.class);
        register(JacksonObjectMapperProvider.class);

        register(new ConfigurationBinder(configuration));

        register(TechnicalExceptionMapper.class);
        register(CodifiedExceptionMapper.class);

        register(new AbstractBinder() {
            @Override
            protected void configure() {
//                bind(TechnicalExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
//                bind(CodifiedExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
                bind(ValidationExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
                bind(ErrorBodyResBodyWriter.class).to(MessageBodyWriter.class).in(Singleton.class);
            }
        });
    }

}
