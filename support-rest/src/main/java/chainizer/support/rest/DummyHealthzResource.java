package chainizer.support.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/healthz")
public class DummyHealthzResource {

    @GET
    public Response check() {
        return Response.ok("OK").type(MediaType.TEXT_PLAIN_TYPE).build();
    }

}
