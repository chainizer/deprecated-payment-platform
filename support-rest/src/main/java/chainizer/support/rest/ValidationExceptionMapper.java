package chainizer.support.rest;

import chainizer.support.core.TechnicalErrorMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.List;

public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

    private final static Logger LOGGER = LoggerFactory.getLogger(ValidationExceptionMapper.class);

    @Context
    private Configuration config;

    @Context
    private javax.inject.Provider<Request> request;

    @Override
    public Response toResponse(ValidationException exception) {
        LOGGER.trace("a validation exception has been thrown", exception);

        final ConstraintViolationException cve = (ConstraintViolationException) exception;
        final Response.ResponseBuilder response = Response.status(TechnicalErrorMsg.INPUT_NOT_VALID.code());

        final List<Variant> variants = Variant.mediaTypes(
                MediaType.TEXT_PLAIN_TYPE,
                MediaType.TEXT_HTML_TYPE,
                MediaType.APPLICATION_XML_TYPE,
                MediaType.APPLICATION_JSON_TYPE).build();

        final Variant variant = request.get().selectVariant(variants);

        response.type(variant != null ? variant.getMediaType() : MediaType.TEXT_PLAIN_TYPE);

        response.entity(
                new ErrorBodyRes(
                        TechnicalErrorMsg.INPUT_NOT_VALID.name(),
                        TechnicalErrorMsg.INPUT_NOT_VALID.format(cve.getMessage())
                )
        );

        return response.build();
    }

}
