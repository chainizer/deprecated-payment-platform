package chainizer.support.rest;

import org.glassfish.jersey.message.MessageUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class ErrorBodyResBodyWriter implements MessageBodyWriter<ErrorBodyRes> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return isSupportedMediaType(mediaType) && isSupportedType(type, genericType);
    }

    private boolean isSupportedType(final Class<?> type, final Type genericType) {
        return ErrorBodyRes.class.isAssignableFrom(type);
    }

    private boolean isSupportedMediaType(final MediaType mediaType) {
        return MediaType.TEXT_HTML_TYPE.equals(mediaType) || MediaType.TEXT_PLAIN_TYPE.equals(mediaType);
    }

    @Override
    public void writeTo(ErrorBodyRes errorBodyRes, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        final boolean isPlain = MediaType.TEXT_PLAIN_TYPE.getSubtype().equals(mediaType.getSubtype());
        final StringBuilder builder = new StringBuilder();

        // Root <div>
        if (!isPlain) {
        }

        if (!isPlain) {
            builder.append("<div class=\"rest-error\">");

            builder.append("<p>");
            builder.append("<span class=\"rest-error-code-label\">code</span>");
            builder.append("<span class=\"rest-error-code\">");
            builder.append(errorBodyRes.getCode());
            builder.append("</span>");
            builder.append("</p>");

            builder.append("<p>");
            builder.append("<span class=\"rest-error-message-label\">message</span>");
            builder.append("<span class=\"rest-error-message\">");
            builder.append(errorBodyRes.getMessage());
            builder.append("</span>");
            builder.append("</p>");

            builder.append("</div>");
        } else {
            builder.append(errorBodyRes.getCode());
            builder.append(" - ");
            builder.append(errorBodyRes.getMessage());
        }

        entityStream.write(builder.toString().getBytes(MessageUtils.getCharset(mediaType)));
        entityStream.flush();
    }

}
