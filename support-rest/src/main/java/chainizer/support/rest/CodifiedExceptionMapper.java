package chainizer.support.rest;

import chainizer.support.core.CodifiedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.*;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.List;

public class CodifiedExceptionMapper implements ExceptionMapper<CodifiedException> {

    private final static Logger LOGGER = LoggerFactory.getLogger(CodifiedExceptionMapper.class);

    @Context
    private javax.inject.Provider<Request> request;

    @Override
    public Response toResponse(CodifiedException exception) {

        if (exception.getCodifiedMessage().code() >= 500 && exception.getCodifiedMessage().code() < 600) {
            LOGGER.warn("a codified exception has been thrown", exception);
        } else {
            LOGGER.trace("a codified exception has been thrown", exception);
        }

        final Response.ResponseBuilder response = Response.status(
                exception.getCodifiedMessage().code()
        );

        final List<Variant> variants = Variant.mediaTypes(
                MediaType.APPLICATION_XML_TYPE,
                MediaType.APPLICATION_JSON_TYPE).build();

        final Variant variant = request.get().selectVariant(variants);

        response.type(variant != null ? variant.getMediaType() : MediaType.APPLICATION_JSON_TYPE);

        response.entity(
                new ErrorBodyRes(
                        exception.getCodifiedMessage().name(),
                        exception.getMessage()
                )
        );

        return response.build();
    }

}
