package chainizer.support.rest;

import chainizer.support.container.Container;
import org.glassfish.jersey.internal.inject.InjectionManager;
import org.glassfish.jersey.server.spi.ComponentProvider;

import javax.ws.rs.ext.Provider;
import java.util.Set;

@Provider
public class ApplicationProvider implements ComponentProvider {

    @Override
    public void initialize(InjectionManager injectionManager) {
        Container.get().configure(
                injectionManager::getInstance,
                injectionManager::inject,
                injectionManager::register
        );
    }

    @Override
    public boolean bind(Class<?> component, Set<Class<?>> providerContracts) {
        return false;
    }

    @Override
    public void done() {
        Container.get().start();
    }

}
