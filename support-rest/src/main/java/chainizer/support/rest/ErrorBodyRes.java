package chainizer.support.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "error")
public class ErrorBodyRes {

    private final String code;

    private final String message;

    public ErrorBodyRes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ErrorBodyRes{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

}
