package chainizer.support.rest;

import chainizer.support.core.TechnicalErrorMsg;
import org.glassfish.jersey.spi.ExtendedExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ValidationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.*;
import java.util.List;


public class TechnicalExceptionMapper implements ExtendedExceptionMapper<RuntimeException> {

    private final static Logger LOGGER = LoggerFactory.getLogger(TechnicalExceptionMapper.class);

    @Context
    private javax.inject.Provider<Request> request;

    @Override
    public Response toResponse(RuntimeException exception) {
        LOGGER.error("a runtime exception has been thrown", exception);

        final Response.ResponseBuilder response = Response.serverError();

        final List<Variant> variants = Variant.mediaTypes(
                MediaType.APPLICATION_XML_TYPE,
                MediaType.APPLICATION_JSON_TYPE).build();

        final Variant variant = request.get().selectVariant(variants);

        response.type(variant != null ? variant.getMediaType() : MediaType.APPLICATION_JSON_TYPE);

        response.entity(
                new ErrorBodyRes(
                        TechnicalErrorMsg.UNEXPECTED_EXCEPTION.name(),
                        TechnicalErrorMsg.UNEXPECTED_EXCEPTION.format()
                )
        );

        return response.build();
    }

    @Override
    public boolean isMappable(RuntimeException exception) {
        return !WebApplicationException.class.isInstance(exception) && !ValidationException.class.isInstance(exception);
    }
}
