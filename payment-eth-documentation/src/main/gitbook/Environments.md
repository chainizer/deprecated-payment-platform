# Environments

The payment platform for ethereum exposes two environments:

- kovan
- main

## Kovan

Kovan is a test network used during development, intergration or testing purpose.
Obviously, you are strongly recommended to use this environment! ;)

## Main

The main network is not yet deployed. 