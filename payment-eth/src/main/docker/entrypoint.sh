#!/usr/bin/env sh

VM_PROPERTIES=""

parseVmOptions() {
  if [ -f $1 ]; then
    while read LINE || [[ -n "$LINE" ]]; do
      LINE="${LINE#"${LINE%%[![:space:]]*}"}"
      if [ ${#LINE} -gt 0 ] && [ ! ${LINE:0:1} == '#' ]; then
        VM_PROPERTIES="$VM_PROPERTIES $LINE"
      fi
    done < $1
  fi
}

parseVmOptions payment-eth.vmoptions

ARGS="$@ --config /opt/payment-eth/payment-eth.properties --environment ${CHAINIZER_ENVIRONMENT:=staging}"

echo "COMMAND: java ${VM_PROPERTIES} -jar payment-eth.jar ${ARGS}"

exec java ${VM_PROPERTIES} -jar payment-eth.jar ${ARGS}