package chainizer.payment.eth.cli;

import chainizer.payment.eth.rest.PaymentResource;
import chainizer.support.cli.Command;
import chainizer.support.container.Container;
import chainizer.support.core.ShutdownHandlers;
import chainizer.support.core.TechnicalException;
import chainizer.support.mongo.Mongo;
import chainizer.support.rest.JaxRs;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import io.netty.channel.Channel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Parameters(commandNames = "rest", commandDescription = "start the REST interface")
public class RestCommand implements Command {

    private final static Logger LOGGER = LoggerFactory.getLogger(RestCommand.class);

    @Parameter(help = true, names = "-h --help")
    @SuppressWarnings("unused")
    private boolean help;

    @Parameter(names = {"-e", "--environment"}, description = "the environment", required = true)
    @SuppressWarnings("unused")
    private String environment;

    @Parameter(names = {"-c", "--config"}, description = "config files")
    private List<File> configurations = new ArrayList<>();

    @Parameter(names = {"-u", "--uri"}, description = "the server URI")
    @SuppressWarnings("unused")
    private String uri = "http://0.0.0.0:9000/";

    @Override
    public void run() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        List<URL> urls = new ArrayList<>();
        urls.add(RestCommand.class.getResource("/payment-eth.properties"));
        for (File configurationFile : configurations) {
            try {
                urls.add(configurationFile.toURI().toURL());
            } catch (MalformedURLException e) {
                throw new TechnicalException(e);
            }
        }

        Channel server = JaxRs.configure()
                .environment(environment)
                .configuration(urls.toArray(new URL[0]))
                .register(PaymentResource.class)
                .health(true)
                .uri(uri)
                .serve();

        ShutdownHandlers.get()
                .add(() -> {
                    try {
                        server.close().get(5, TimeUnit.MINUTES);
                    } catch (InterruptedException | ExecutionException | TimeoutException e) {
                        LOGGER.info("unable to close the server", e);
                    }
                })
                .add(Container.get()::stop)
                .add(Mongo::close)
                .registerHook();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("environment", environment)
                .append("configurations", configurations)
                .toString();
    }

}