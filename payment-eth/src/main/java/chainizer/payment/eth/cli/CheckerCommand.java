package chainizer.payment.eth.cli;

import chainizer.payment.eth.payment.checker.CheckPaymentsCmd;
import chainizer.support.axon.CommandGateway;
import chainizer.support.cli.Command;
import chainizer.support.container.Container;
import chainizer.support.core.TechnicalException;
import chainizer.support.rest.JaxRs;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Parameters(commandNames = "checker", commandDescription = "start the checker task")
public class CheckerCommand implements Command {

    private final static Logger LOGGER = LoggerFactory.getLogger(CheckerCommand.class);

    @Parameter(help = true, names = "-h --help")
    @SuppressWarnings("unused")
    private boolean help;

    @Parameter(names = {"-e", "--environment"}, description = "the environment", required = true)
    @SuppressWarnings("unused")
    private String environment;

    @Parameter(names = {"-c", "--config"}, description = "config files")
    private List<File> configurations = new ArrayList<>();

    @Parameter(names = {"-u", "--uri"}, description = "the server URI")
    @SuppressWarnings("unused")
    private String uri = "http://0.0.0.0:9000/";

    private AtomicBoolean running = new AtomicBoolean(true);

    @Override
    public void run() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        List<URL> urls = new ArrayList<>();
        urls.add(RestCommand.class.getResource("/payment-eth.properties"));
        for (File configurationFile : configurations) {
            try {
                urls.add(configurationFile.toURI().toURL());
            } catch (MalformedURLException e) {
                throw new TechnicalException(e);
            }
        }

        JaxRs.configure()
                .environment(environment)
                .configuration(urls.toArray(new URL[0]))
                .health(true)
                .uri(uri)
                .serve();

        CommandGateway commandGateway = Container.get().get(CommandGateway.class);
        Configuration configuration = Container.get().get(Configuration.class);

        while (running.get()) {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            boolean result = commandGateway.sendAndWait(new CheckPaymentsCmd());
            stopWatch.stop();
            LOGGER.info("CheckPaymentsCmd done in [{}] ms, result [{}]", stopWatch.getTime(TimeUnit.MILLISECONDS), result);
            int delayInSec = configuration.getInt("chainizer.payment.eth.checker.executor.delayInSec", 30);
            int delayInMil = delayInSec * 1000;
            if (stopWatch.getTime(TimeUnit.MILLISECONDS) < delayInMil) {
                try {
                    Thread.sleep(delayInMil - stopWatch.getTime(TimeUnit.MILLISECONDS));
                } catch (InterruptedException e) {
                    LOGGER.debug("infinite loop interrupted", e);
                    running.set(false);
                }
            }
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("environment", environment)
                .append("configurations", configurations)
                .toString();
    }
}
