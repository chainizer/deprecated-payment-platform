package chainizer.payment.eth.support;

import org.jasypt.util.digest.Digester;
import org.jasypt.util.text.StrongTextEncryptor;

import java.nio.charset.Charset;

public class Crypto {

    public static String encrypt(String value, String password) {
        StrongTextEncryptor encryptor = new StrongTextEncryptor();
        encryptor.setPassword(password);
        return encryptor.encrypt(value);
    }

    public static String decrypt(String value, String password) {
        StrongTextEncryptor encryptor = new StrongTextEncryptor();
        encryptor.setPassword(password);
        return encryptor.decrypt(value);
    }

    public static String toChallenge(String token) {
        Digester digester = new Digester("SHA-256");
        byte[] challengeInByte = digester.digest(token.getBytes(Charset.forName("UTF-8")));
        return new String(challengeInByte, Charset.forName("UTF-8"));
    }

}
