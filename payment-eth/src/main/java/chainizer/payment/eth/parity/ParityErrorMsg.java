package chainizer.payment.eth.parity;

import chainizer.support.core.CodifiedMessage;

public enum ParityErrorMsg implements CodifiedMessage {
    PARITY_REMOTE_CALL_FAILED(500, "The remote call failed."),
    PARITY_METHOD_FAILED(400, "The method failed - [%s] - [%s].");

    private final int code;

    private final String template;

    ParityErrorMsg(int code, String template) {
        this.template = template;
        this.code = code;
    }

    @Override
    public String format(Object... params) {
        return String.format(template, params);
    }

    @Override
    public int code() {
        return code;
    }

}
