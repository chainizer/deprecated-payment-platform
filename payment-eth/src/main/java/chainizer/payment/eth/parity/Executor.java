package chainizer.payment.eth.parity;

import chainizer.support.core.TechnicalException;
import org.web3j.protocol.core.Response;

import java.io.IOException;

public class Executor {

    public static <T extends Response<?>> T call(Call<T> supplier) throws CallException {
        try {
            T response = supplier.get();

            if (response.hasError()) {
                throw new CallException(response.getError());
            }

            return response;
        } catch (IOException e) {
            throw new TechnicalException(e, ParityErrorMsg.PARITY_REMOTE_CALL_FAILED);
        }
    }

}
