package chainizer.payment.eth.parity;

import org.web3j.protocol.core.Response;

import java.io.IOException;

public interface Call<T extends Response<?>> {
    T get() throws IOException;
}
