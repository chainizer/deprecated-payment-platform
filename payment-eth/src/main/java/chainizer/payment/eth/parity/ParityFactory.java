package chainizer.payment.eth.parity;

import org.apache.commons.configuration2.Configuration;
import org.glassfish.hk2.api.Factory;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;

import javax.inject.Inject;

public class ParityFactory implements Factory<Parity> {

    private final Configuration configuration;

    @Inject
    public ParityFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Parity provide() {
        return Parity.build(
                new HttpService(
                        configuration.getString("chainizer.payment.eth.parity.endpoint")
                )
        );
    }

    @Override
    public void dispose(Parity instance) {
    }

}
