package chainizer.payment.eth.parity;

import chainizer.support.core.CodifiedException;
import org.web3j.protocol.core.Response.Error;

public class CallException extends CodifiedException {

    public CallException(Error error) {
        super(ParityErrorMsg.PARITY_METHOD_FAILED, error.getCode(), error.getMessage());
    }

}