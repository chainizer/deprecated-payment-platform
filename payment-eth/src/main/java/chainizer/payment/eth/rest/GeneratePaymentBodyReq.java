package chainizer.payment.eth.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

public class GeneratePaymentBodyReq {

    @URL
    @NotBlank
    @Size(max = 512)
    private String callbackUrl;

    public GeneratePaymentBodyReq() {
    }

    public GeneratePaymentBodyReq(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("callbackUrl", callbackUrl)
                .toString();
    }

}
