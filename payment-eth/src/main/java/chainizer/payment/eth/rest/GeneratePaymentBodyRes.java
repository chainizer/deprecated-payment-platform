package chainizer.payment.eth.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;

public class GeneratePaymentBodyRes {

    private String address;

    private String token;

    public GeneratePaymentBodyRes() {
    }

    public GeneratePaymentBodyRes(String address, String token) {
        this.address = address;
        this.token = token;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("token", token)
                .toString();
    }
}
