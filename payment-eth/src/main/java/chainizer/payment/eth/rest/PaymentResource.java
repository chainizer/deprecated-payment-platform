package chainizer.payment.eth.rest;

import chainizer.payment.eth.payment.GeneratePaymentCmd;
import chainizer.payment.eth.payment.query.GetPaymentReq;
import chainizer.payment.eth.payment.query.GetPaymentRes;
import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmd;
import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmdRes;
import chainizer.payment.eth.support.Crypto;
import chainizer.support.axon.CommandGateway;
import chainizer.support.axon.QueryGateway;
import chainizer.support.core.TechnicalException;
import org.glassfish.jersey.server.ContainerRequest;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.utils.Numeric;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Path("/api/payment")
public class PaymentResource {

    private final CommandGateway commandGateway;

    private final QueryGateway queryGateway;

    @Context
    private Provider<ContainerRequest> request;

    @Inject
    public PaymentResource(CommandGateway commandGateway, QueryGateway queryGateway) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response generatePayment(@NotNull @Valid GeneratePaymentBodyReq req) {
        try {

            String token = UUID.randomUUID().toString();
            ECKeyPair ecKeyPair = Keys.createEcKeyPair();
            String address = Numeric.prependHexPrefix(Keys.getAddress(ecKeyPair));
            String privateKey = Numeric.toHexStringWithPrefix(ecKeyPair.getPrivateKey());
            String encryptedPrivateKey = Crypto.encrypt(privateKey, token);
            String challenge = Crypto.toChallenge(token);

            GeneratePaymentCmd cmd = new GeneratePaymentCmd(
                    address,
                    encryptedPrivateKey,
                    challenge,
                    req.getCallbackUrl()
            );
            commandGateway.sendAndWait(cmd);

            return Response
                    .created(URI.create(request.get().getRequestUri().getPath() + "/" + address))
                    .entity(new GeneratePaymentBodyRes(address, token))
                    .build();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchProviderException e) {
            throw new TechnicalException(e);
        }
    }

    @GET
    @Path("{address}")
    @Produces({MediaType.APPLICATION_JSON})
    public PaymentBodyRes getPayment(@PathParam("address") String address, @HeaderParam("Authorization") String authorization) {
        if (isBlank(authorization)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED));
        }

        String token = authorization.trim().substring("bearer".length()).trim();

        if (isBlank(token)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED));
        }

        GetPaymentReq req = new GetPaymentReq(address);

        GetPaymentRes res = queryGateway.sendAndWait(req, GetPaymentRes.class, token);

        if (res == null) {
            throw new NotFoundException();
        }

        return new PaymentBodyRes(res);
    }

    @POST
    @Path("{address}/withdraw")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response withdraw(
            @PathParam("address") String address,
            @HeaderParam("Authorization") String authorization,
            @NotNull @Valid WithdrawPaymentBodyReq req
    ) {
        if (isBlank(authorization)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED));
        }

        String token = authorization.trim().substring("bearer".length()).trim();

        if (isBlank(token)) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED));
        }

        WithdrawPaymentCmd cmd = new WithdrawPaymentCmd(
                address,
                req.getTo(),
                req.getMessage()
        );

        WithdrawPaymentCmdRes cmdRes = commandGateway.sendAndWait(cmd, token);
        if (cmdRes == null) {
            throw new NotFoundException();
        }

        return Response
                .created(URI.create(request.get().getRequestUri().getPath() + "/" + address))
                .entity(new WithdrawPaymentBodyRes(cmdRes.getUserHash()))
                .build();
    }

}
