package chainizer.payment.eth.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

public class WithdrawPaymentBodyRes {

    @NotNull
    private String hash;

    public WithdrawPaymentBodyRes() {
    }

    public WithdrawPaymentBodyRes(@NotNull String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("hash", hash)
                .toString();
    }

}
