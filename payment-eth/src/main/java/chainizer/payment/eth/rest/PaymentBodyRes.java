package chainizer.payment.eth.rest;

import chainizer.payment.eth.payment.query.GetPaymentRes;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.stream.Collectors;

public class PaymentBodyRes {

    private String address;
    private String createdOn;
    private String callbackUrl;
    private String balance;
    private List<Event> events;

    public PaymentBodyRes() {
    }

    public PaymentBodyRes(GetPaymentRes getPaymentRes) {
        this.address = getPaymentRes.getAddress();
        this.createdOn = getPaymentRes.getCreatedOn().toString();
        this.callbackUrl = getPaymentRes.getCallbackUrl();
        this.balance = getPaymentRes.getBalance();
        this.events = getPaymentRes.getEvents().stream().map(Event::new).collect(Collectors.toList());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public static class Event {
        private String type;
        private String attemptedOn;
        private String error;

        public Event() {
        }

        public Event(GetPaymentRes.Event event) {
            this.type = event.getType();
            this.attemptedOn = event.getAttemptedOn().toString();
            this.error = event.getError();
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAttemptedOn() {
            return attemptedOn;
        }

        public void setAttemptedOn(String attemptedOn) {
            this.attemptedOn = attemptedOn;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .appendSuper(super.toString())
                    .append("type", type)
                    .append("attemptedOn", attemptedOn)
                    .append("error", error)
                    .toString();
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("createdOn", createdOn)
                .append("callbackUrl", callbackUrl)
                .append("balance", balance)
                .append("events", events)
                .toString();
    }
}
