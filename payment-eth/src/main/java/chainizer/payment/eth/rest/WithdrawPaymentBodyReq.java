package chainizer.payment.eth.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

public class WithdrawPaymentBodyReq {

    @NotNull
    private String to;

    private String message;

    public WithdrawPaymentBodyReq() {
    }

    public WithdrawPaymentBodyReq(String to, String message) {
        this.to = to;
        this.message = message;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("to", to)
                .append("message", message)
                .toString();
    }
}
