package chainizer.payment.eth;

import chainizer.payment.eth.parity.ParityFactory;
import chainizer.payment.eth.payment.checker.CheckRecordsTask;
import chainizer.payment.eth.payment.checker.PaymentCheckerHandler;
import chainizer.payment.eth.payment.query.PaymentQueryHandler;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.payment.withdraw.PaymentWithdrawHandler;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.web3j.protocol.parity.Parity;

import javax.inject.Singleton;

public class AllInOnBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bindFactory(ParityFactory.class).to(Parity.class);
        bind(CheckRecordsTask.class).to(CheckRecordsTask.class).in(Singleton.class);
        bind(Repository.class).to(Repository.class).in(Singleton.class);
        bind(PaymentCheckerHandler.class).to(PaymentCheckerHandler.class).in(Singleton.class);
        bind(PaymentQueryHandler.class).to(PaymentQueryHandler.class).in(Singleton.class);
        bind(PaymentWithdrawHandler.class).to(PaymentWithdrawHandler.class).in(Singleton.class);
    }

}
