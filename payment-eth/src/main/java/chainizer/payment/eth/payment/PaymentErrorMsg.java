package chainizer.payment.eth.payment;

import chainizer.support.core.CodifiedMessage;

public enum PaymentErrorMsg implements CodifiedMessage {
    BAD_CALLBACK_URL(400, "The URL is not valid: [%s]."),
    BAD_CALLBACK_HTTP_RESPONSE(400, "The HTTP status is not valid: [%s]."),
    CALLBACK_CONNECTION_FAILED(400, "The HTTP connection failed."),
    NOT_ENOUGH_FUND(400, "The balance is not enough: [%s], [%s] are missing."),
    PAYMENT_NOT_FOUND(404, "Unable to found with address [%s]."),
    UNABLE_TO_UNLOCK(500, "Unable to unlock the address [%s].");

    private final int code;

    private final String template;

    PaymentErrorMsg(int code, String template) {
        this.template = template;
        this.code = code;
    }

    @Override
    public String format(Object... params) {
        return String.format(template, params);
    }

    @Override
    public int code() {
        return code;
    }

}

