package chainizer.payment.eth.payment;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;

import java.time.Instant;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

public class Payment {

    @AggregateIdentifier
    private String address;

    @SuppressWarnings("UnusedDeclaration")
    private Payment() {
    }

    @CommandHandler
    @SuppressWarnings("UnusedDeclaration")
    public Payment(GeneratePaymentCmd cmd) {
        apply(new PaymentGeneratedEvt(
                cmd.getAddress(),
                cmd.getEncryptedPrivateKey(),
                cmd.getChallenge(),
                cmd.getCallbackUrl(),
                Instant.now()
        ));
    }

    @EventSourcingHandler
    @SuppressWarnings("UnusedDeclaration")
    private void on(PaymentGeneratedEvt evt) {
        this.address = evt.getAddress();
    }

    @CommandHandler
    @SuppressWarnings("UnusedDeclaration")
    public void handle(LogCallResultCmd cmd) {
        if (cmd.isSuccessful()) {
            apply(new NotificationSucceedEvt(
                    cmd.getAddress(),
                    cmd.getAttemptedOn()
            ));
        } else {
            apply(new NotificationFailedEvt(
                    cmd.getAddress(),
                    cmd.getAttemptedOn(),
                    cmd.getError()
            ));
        }
    }

    @CommandHandler
    @SuppressWarnings("UnusedDeclaration")
    public void handle(LogWithdrawCmd cmd) {
        if (cmd.isSuccessful()) {
            apply(new WithdrawSucceedEvt(
                    cmd.getAddress(),
                    cmd.getAttemptedOn()
            ));
        } else {
            apply(new WithdrawFailedEvt(
                    cmd.getAddress(),
                    cmd.getAttemptedOn(),
                    cmd.getError()
            ));
        }
    }

}
