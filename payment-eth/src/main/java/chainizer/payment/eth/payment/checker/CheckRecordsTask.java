package chainizer.payment.eth.payment.checker;

import chainizer.payment.eth.payment.repository.Repository;
import chainizer.support.axon.CommandGateway;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.parity.Parity;

import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CheckRecordsTask implements Callable<Boolean> {

    private final static Logger LOGGER = LoggerFactory.getLogger(CheckRecordsTask.class);

    private final static int DEFAULT_EXECUTOR_SIZE = 2;
    private final static int DEFAULT_AWAIT_VALUE = 1;
    private final static String DEFAULT_AWAIT_UNIT = TimeUnit.HOURS.name();

    private static final long DEFAULT_MIN_CREATED_VALUE = 2;
    private static final String DEFAULT_MIN_CREATED_UNIT = ChronoUnit.DAYS.name();

    private static final long DEFAULT_MAX_NOTIFIED_VALUE = 30;
    private static final String DEFAULT_MAX_NOTIFIED_UNIT = ChronoUnit.MINUTES.name();

    private final Configuration configuration;

    private final Parity parity;

    private final CommandGateway commandGateway;

    private final Repository repository;

    @Inject
    public CheckRecordsTask(Configuration configuration, Parity parity, CommandGateway commandGateway, Repository repository) {
        this.configuration = configuration;
        this.parity = parity;
        this.commandGateway = commandGateway;
        this.repository = repository;
    }

    @Override
    public Boolean call() {
        try {

            ExecutorService executor = Executors.newFixedThreadPool(
                    configuration.getInt("chainizer.payment.eth.checker.executor.size", DEFAULT_EXECUTOR_SIZE)
            );

            Instant minCreatedOn = Instant.now().minus(
                    configuration.getLong("chainizer.payment.eth.checker.min_created.value", DEFAULT_MIN_CREATED_VALUE),
                    ChronoUnit.valueOf(
                            configuration.getString("chainizer.payment.eth.checker.min_created.unit", DEFAULT_MIN_CREATED_UNIT)
                    )
            );

            Instant maxNotifiedOn = Instant.now().minus(
                    configuration.getLong("chainizer.payment.eth.checker.max_notified.value", DEFAULT_MAX_NOTIFIED_VALUE),
                    ChronoUnit.valueOf(
                            configuration.getString("chainizer.payment.eth.checker.max_notified.unit", DEFAULT_MAX_NOTIFIED_UNIT)
                    )
            );

            repository.listActiveRecords(
                    minCreatedOn,
                    maxNotifiedOn,
                    record -> executor.execute(new CheckRecordTask(
                            configuration,
                            parity,
                            commandGateway,
                            record
                    ))
            );

            executor.shutdown();

            try {
                executor.awaitTermination(
                        configuration.getInt("chainizer.payment.eth.checker.executor.await.value", DEFAULT_AWAIT_VALUE),
                        TimeUnit.valueOf(
                                configuration.getString("chainizer.payment.eth.checker.executor.await.unit", DEFAULT_AWAIT_UNIT)
                        )
                );
            } catch (InterruptedException e) {
                LOGGER.warn("unable to complete the tasks", e);
                return false;
            }

        } catch (Exception e) {
            LOGGER.warn("the checker failed", e);
            return false;
        }

        return true;
    }

}

