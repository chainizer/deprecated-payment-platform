package chainizer.payment.eth.payment.repository;

import chainizer.support.mongo.Mongo;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Updates;
import org.apache.commons.configuration2.Configuration;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static chainizer.support.core.Utils.convert;

public class Repository {

    private final static Logger LOGGER = LoggerFactory.getLogger(Repository.class);

    private final Configuration configuration;

    @Inject
    public Repository(Configuration configuration) {
        this.configuration = configuration.subset("chainizer.payment.eth.repository");
    }

    private Record toRecord(Document document) {
        List<Event> events = document
                .get("events", new ArrayList<Document>())
                .stream()
                .map(d -> new Event(
                        EventType.valueOf(d.getString("type")),
                        convert(d.getLong("attemptedOn"), null, Instant::ofEpochMilli),
                        d.getString("error")
                ))
                .collect(Collectors.toList());

        return new Record(
                document.getString("address"),
                document.getString("encryptedPrivateKey"),
                document.getString("challenge"),
                document.getString("callbackUrl"),
                convert(document.getLong("createdOn"), null, Instant::ofEpochMilli),
                document.getBoolean("notified"),
                convert(document.getLong("lastNotificationAttemptedOn"), null, Instant::ofEpochMilli),
                document.getBoolean("withdrawn"),
                events
        );
    }

    public MongoCollection<Document> getPayments() {
        MongoDatabase database = Mongo.getClient(configuration).getDatabase(configuration.getString("mongodb.database"));

        MongoCollection<Document> collection = database.getCollection(configuration.getString("mongodb.collection.payments"));

        collection.createIndex(
                new BasicDBObject("address", 1),
                new IndexOptions().unique(true)
        );

        collection.createIndex(
                new BasicDBObject("challenge", 1)
        );

        collection.createIndex(
                Indexes.compoundIndex(Arrays.asList(
                        new BasicDBObject("createdOn", 1),
                        new BasicDBObject("notified", 1),
                        new BasicDBObject("lastNotificationAttemptedOn", 1),
                        new BasicDBObject("withdrawn", 1)
                ))
        );

        return collection;
    }

    private Document toDocument(Event event) {
        Document document = new Document();
        document.put("type", event.getType().name());
        document.put("attemptedOn", convert(event.getAttemptedOn(), null, Instant::toEpochMilli));
        document.put("successful", event.isSuccessful());
        document.put("error", event.getError());
        return document;
    }

    private Document toDocument(Record record) {
        Document document = new Document();
        document.put("address", record.getAddress());
        document.put("encryptedPrivateKey", record.getEncryptedPrivateKey());
        document.put("challenge", record.getChallenge());
        document.put("callbackUrl", record.getCallbackUrl());
        document.put("createdOn", convert(record.getCreatedOn(), null, Instant::toEpochMilli));
        document.put("notified", record.isNotified());
        document.put("lastNotificationAttemptedOn", convert(record.getLastNotificationAttemptedOn(), null, Instant::toEpochMilli));
        document.put("withdrawn", record.isWithdrawn());
        document.put("events",
                record.getEvents().stream().map(this::toDocument).collect(Collectors.toList())
        );
        return document;
    }

    public void save(Record record) {
        Document document = toDocument(record);
        LOGGER.debug("save {}", document);
        getPayments().insertOne(document);
    }

    public void addEvent(String address, Event event) {
        Document document = toDocument(event);
        LOGGER.debug("add {}", document);
        String timeFieldName = event.getType() == EventType.notification ? "notified" : "withdrawn";
        String lastFieldName = event.getType() == EventType.notification ? "Notification" : "Withdraw";
        getPayments().updateOne(
                Filters.eq("address", address),
                Updates.combine(
                        Updates.push("events", document),
                        Updates.set(timeFieldName, event.isSuccessful()),
                        Updates.set("last" + lastFieldName + "AttemptedOn", document.getLong("attemptedOn"))
                )
        );
    }

    public Record getByAddress(String address) {
        Document document = getPayments().find(Filters.eq("address", address)).first();
        if (document == null) {
            return null;
        }

        return toRecord(document);
    }

    public Record getByAddress(String address, String challenge) {
        Document document = getPayments().find(
                Filters.and(
                        Filters.eq("address", address),
                        Filters.eq("challenge", challenge)
                )
        ).first();
        if (document == null) {
            return null;
        }

        return toRecord(document);
    }

    public void listActiveRecords(Instant minCreatedOn, Instant maxNotifiedOn, Block<Record> block) {
        getPayments()
                .find(
                        Filters.and(
                                Filters.eq("notified", false),
                                Filters.eq("withdrawn", false),
                                Filters.gte("createdOn", minCreatedOn.toEpochMilli()),
                                Filters.or(
                                        Filters.eq("lastNotificationAttemptedOn", null),
                                        Filters.lte("lastNotificationAttemptedOn", maxNotifiedOn.toEpochMilli())
                                )

                        )
                )
                .map(this::toRecord)
                .forEach(block);
    }

}
