package chainizer.payment.eth.payment;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;

public class NotificationFailedEvt {

    private final String address;

    private final Instant failedOn;

    private final String reason;

    public NotificationFailedEvt(String address, Instant failedOn, String reason) {
        this.address = address;
        this.failedOn = failedOn;
        this.reason = reason;
    }

    public String getAddress() {
        return address;
    }

    public Instant getFailedOn() {
        return failedOn;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("failedOn", failedOn)
                .append("reason", reason)
                .toString();
    }
}
