package chainizer.payment.eth.payment.withdraw;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;

public class WithdrawPaymentCmdRes {

    private final String feeHash;

    private final String userHash;

    private final Instant attemptedOn;

    private final boolean successful;

    private final String error;

    public WithdrawPaymentCmdRes(String feeHash, String userHash) {
        this.feeHash = feeHash;
        this.userHash = userHash;
        this.attemptedOn = Instant.now();
        this.successful = true;
        this.error = null;
    }

    public WithdrawPaymentCmdRes(String error) {
        this.feeHash = null;
        this.userHash = null;
        this.attemptedOn = Instant.now();
        this.successful = false;
        this.error = error;
    }

    public String getFeeHash() {
        return feeHash;
    }

    public String getUserHash() {
        return userHash;
    }

    public Instant getAttemptedOn() {
        return attemptedOn;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getError() {
        return error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("feeHash", feeHash)
                .append("userHash", userHash)
                .append("attemptedOn", attemptedOn)
                .append("successful", successful)
                .append("error", error)
                .toString();
    }
}
