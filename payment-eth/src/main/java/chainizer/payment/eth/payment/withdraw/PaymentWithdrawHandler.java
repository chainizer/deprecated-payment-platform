package chainizer.payment.eth.payment.withdraw;

import chainizer.payment.eth.parity.Executor;
import chainizer.payment.eth.payment.LogWithdrawCmd;
import chainizer.payment.eth.payment.NotEnoughFundException;
import chainizer.payment.eth.payment.UnableToUnlockException;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.support.Crypto;
import chainizer.support.axon.CommandGateway;
import chainizer.support.core.CodifiedException;
import chainizer.support.core.TechnicalErrorMsg;
import chainizer.support.core.TechnicalException;
import org.apache.commons.configuration2.Configuration;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.messaging.annotation.MetaDataValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.parity.Parity;
import org.web3j.utils.Convert;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.BigInteger;

public class PaymentWithdrawHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(PaymentWithdrawHandler.class);

    private final Configuration configuration;

    private final Parity parity;

    private final CommandGateway commandGateway;

    private final Repository repository;

    @Inject
    public PaymentWithdrawHandler(Configuration configuration, Repository repository, CommandGateway commandGateway, Parity parity) {
        this.configuration = configuration;
        this.commandGateway = commandGateway;
        this.parity = parity;
        this.repository = repository;
    }

    private BigInteger estimateGas(BigInteger value) throws CodifiedException {
        Transaction feeTx = Transaction.createEtherTransaction(
                null,
                null,
                null,
                null,
                null,
                value
        );
        return Executor.call(
                () -> parity.ethEstimateGas(feeTx).send()
        ).getAmountUsed();
    }

    private String send(Transaction transaction, String password) throws CodifiedException {

        boolean unlocked = Executor.call(
                () -> parity.personalUnlockAccount(transaction.getFrom(), password).send()
        ).accountUnlocked();

        if (!unlocked) {
            throw new UnableToUnlockException(transaction.getFrom());
        }

        return Executor.call(
                () -> parity.ethSendTransaction(transaction).send()
        ).getTransactionHash();

    }

    private void activate(Record record, String password) throws CodifiedException {
        String privateKey = Crypto.decrypt(record.getEncryptedPrivateKey(), password);
        Executor.call(
                () -> parity.parityNewAccountFromSecret(privateKey, password).send()
        );
    }

    private void deactivate(String address, String password) {
        try {
            Executor.call(
                    () -> parity.parityKillAccount(address, password).send()
            );
        } catch (Exception e) {
            LOGGER.debug("unable to deactivate", e);
        }
    }

    @CommandHandler
    public WithdrawPaymentCmdRes handle(WithdrawPaymentCmd cmd, @MetaDataValue(value = "token", required = true) String token) {
        LOGGER.debug("withdraw: {}", cmd.getAddress());
        BigDecimal minFee = Convert.toWei(configuration.getString("chainizer.payment.eth.checker.fee.min"), Convert.Unit.ETHER);
        BigDecimal maxFee = Convert.toWei(configuration.getString("chainizer.payment.eth.checker.fee.max"), Convert.Unit.ETHER);
        BigInteger rateFee = configuration.getBigInteger("chainizer.payment.eth.checker.fee.rate");
        String challenge = Crypto.toChallenge(token);


        Record record = repository.getByAddress(cmd.getAddress(), challenge);
        if (record == null) {
            return null;
        }

        try {

            String coinbase = Executor.call(
                    () -> parity.ethCoinbase().send()
            ).getAddress();

            BigInteger gasPrice = Executor.call(
                    () -> parity.ethGasPrice().send()
            ).getGasPrice();

            BigInteger balance = Executor.call(
                    () -> parity.ethGetBalance(cmd.getAddress(), DefaultBlockParameterName.LATEST).send()
            ).getBalance();

            boolean isFeeApplied = balance.compareTo(minFee.toBigInteger()) > 0;
            boolean isMaxFeeReached = balance.compareTo(maxFee.toBigInteger()) > 0;
            BigInteger taxableAmount = isMaxFeeReached ? maxFee.toBigInteger() : balance;
            LOGGER.debug("fee: [{}, {}], rate: {}", minFee, maxFee, rateFee);
            LOGGER.debug("isFeeApplied: {}, isMaxFeeReached: {}", isFeeApplied, isMaxFeeReached);
            LOGGER.debug("balance {}", balance);
            LOGGER.debug("taxableAmount {}", taxableAmount);
            BigInteger feeValue = isFeeApplied ? taxableAmount.multiply(rateFee).divide(BigInteger.valueOf(100)) : BigInteger.ZERO;
            LOGGER.debug("feeValue {}", feeValue);
            BigInteger feeGaz = estimateGas(feeValue);
            Transaction feeTx = Transaction.createEtherTransaction(
                    cmd.getAddress(),
                    null,
                    gasPrice,
                    feeGaz,
                    coinbase,
                    feeValue
            );
            BigInteger feeCost = feeValue.add(gasPrice.multiply(feeGaz));

            BigInteger grossUserValue = balance.subtract(feeCost);
            BigInteger userGaz = estimateGas(feeValue);
            BigInteger netUserValue = grossUserValue.subtract(gasPrice.multiply(userGaz));
            Transaction userTx = Transaction.createEtherTransaction(
                    cmd.getAddress(),
                    null,
                    gasPrice,
                    userGaz,
                    cmd.getToAddress(),
                    netUserValue
            );

            if (netUserValue.compareTo(BigInteger.ZERO) < 1) {
                throw new NotEnoughFundException(balance, netUserValue.negate());
            }

            activate(record, token);
            String userHash = send(userTx, token);
            String feeHash = null;
            if (isFeeApplied) {
                feeHash = send(feeTx, token);
            }

            LOGGER.info("transactions sent {} / {}", feeHash, userHash);

            WithdrawPaymentCmdRes res = new WithdrawPaymentCmdRes(
                    feeHash,
                    userHash
            );
            commandGateway.send(new LogWithdrawCmd(
                    cmd.getAddress(),
                    cmd.getToAddress(),
                    cmd.getMessage(),
                    res
            ));
            return res;
        } catch (CodifiedException | TechnicalException e) {
            LOGGER.warn("unable to withdraw: {}", e.getMessage());
            WithdrawPaymentCmdRes res = new WithdrawPaymentCmdRes(
                    e.getMessage()
            );
            commandGateway.send(new LogWithdrawCmd(
                    cmd.getAddress(),
                    cmd.getToAddress(),
                    cmd.getMessage(),
                    res
            ));
            return res;
        } catch (Exception e) {
            CodifiedException exception = new CodifiedException(TechnicalErrorMsg.UNEXPECTED_EXCEPTION, e);
            LOGGER.error("unable to withdraw", e);
            WithdrawPaymentCmdRes res = new WithdrawPaymentCmdRes(
                    exception.getMessage()
            );
            commandGateway.send(new LogWithdrawCmd(
                    cmd.getAddress(),
                    cmd.getToAddress(),
                    cmd.getMessage(),
                    res
            ));
            return res;
        } finally {
            deactivate(cmd.getAddress(), token);
        }
    }

}
