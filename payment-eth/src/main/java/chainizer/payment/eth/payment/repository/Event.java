package chainizer.payment.eth.payment.repository;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;

public class Event {

    private EventType type;

    private Instant attemptedOn;

    private boolean successful;

    private String error;

    public Event() {
    }

    public Event(EventType type, Instant attemptedOn, String error) {
        this.type = type;
        this.attemptedOn = attemptedOn;
        this.successful = error == null;
        this.error = error;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public Instant getAttemptedOn() {
        return attemptedOn;
    }

    public void setAttemptedOn(Instant attemptedOn) {
        this.attemptedOn = attemptedOn;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("type", type)
                .append("attemptedOn", attemptedOn)
                .append("successful", successful)
                .append("error", error)
                .toString();
    }

}
