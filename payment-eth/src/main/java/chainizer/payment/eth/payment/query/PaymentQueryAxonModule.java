package chainizer.payment.eth.payment.query;

import chainizer.support.axon.AxonModule;
import org.axonframework.config.Configurer;
import org.axonframework.config.EventHandlingConfiguration;

import javax.inject.Inject;

public class PaymentQueryAxonModule implements AxonModule {

    @Inject
    private PaymentQueryHandler paymentQueryHandler;

    @Override
    public void configure(Configurer configurer) {
        configurer.registerQueryHandler(c -> paymentQueryHandler);
        configurer.registerModule(new EventHandlingConfiguration()
                .registerEventHandler(configuration -> paymentQueryHandler)
        );
    }

}
