package chainizer.payment.eth.payment.query;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

public class GetPaymentReq {

    @NotNull
    private final String address;

    public GetPaymentReq(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .toString();
    }

}
