package chainizer.payment.eth.payment.query;

import chainizer.payment.eth.parity.Executor;
import chainizer.payment.eth.payment.*;
import chainizer.payment.eth.payment.repository.Event;
import chainizer.payment.eth.payment.repository.EventType;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.support.Crypto;
import chainizer.support.core.CodifiedException;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.messaging.annotation.MetaDataValue;
import org.axonframework.queryhandling.QueryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.parity.Parity;

import javax.inject.Inject;
import java.math.BigInteger;
import java.util.stream.Collectors;

public class PaymentQueryHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(PaymentQueryHandler.class);

    private final Repository repository;

    private final Parity parity;

    @Inject
    public PaymentQueryHandler(Repository repository, Parity parity) {
        this.repository = repository;
        this.parity = parity;
    }

    @EventHandler
    @SuppressWarnings("UnusedDeclaration")
    public void on(PaymentGeneratedEvt evt) {
        Record record = new Record(
                evt.getAddress(),
                evt.getEncryptedPrivateKey(),
                evt.getChallenge(),
                evt.getCallbackUrl(),
                evt.getCreatedOn()
        );
        LOGGER.debug("store new record: {}", record);
        repository.save(record);
    }

    @EventHandler
    @SuppressWarnings("UnusedDeclaration")
    public void on(NotificationFailedEvt evt) {
        Event event = new Event(
                EventType.notification,
                evt.getFailedOn(),
                evt.getReason()
        );
        LOGGER.debug("add new event: {}", event);
        repository.addEvent(evt.getAddress(), event);
    }

    @EventHandler
    @SuppressWarnings("UnusedDeclaration")
    public void on(NotificationSucceedEvt evt) {
        Event event = new Event(
                EventType.notification,
                evt.getSucceedOn(),
                null
        );
        LOGGER.debug("add new event: {}", event);
        repository.addEvent(evt.getAddress(), event);
    }

    @EventHandler
    @SuppressWarnings("UnusedDeclaration")
    public void on(WithdrawFailedEvt evt) {
        Event event = new Event(
                EventType.withdraw,
                evt.getFailedOn(),
                evt.getReason()
        );
        LOGGER.debug("add new event: {}", event);
        repository.addEvent(evt.getAddress(), event);
    }

    @EventHandler
    @SuppressWarnings("UnusedDeclaration")
    public void on(WithdrawSucceedEvt evt) {
        Event event = new Event(
                EventType.withdraw,
                evt.getSucceedOn(),
                null
        );
        LOGGER.debug("add new event: {}", event);
        repository.addEvent(evt.getAddress(), event);
    }

    @QueryHandler
    @SuppressWarnings("UnusedDeclaration")
    public GetPaymentRes handle(GetPaymentReq req, @MetaDataValue(value = "token", required = true) String token) throws CodifiedException {
        LOGGER.debug("query: {}", req.getAddress());
        String challenge = Crypto.toChallenge(token);

        Record record = repository.getByAddress(req.getAddress(), challenge);

        if (record == null) {
            return null;
        }

        BigInteger balance = Executor.call(() ->
                parity.ethGetBalance(record.getAddress(), DefaultBlockParameterName.LATEST).send()
        ).getBalance();

        return new GetPaymentRes(
                record.getAddress(),
                record.getCreatedOn(),
                record.getCallbackUrl(),
                balance.toString(10),
                record.getEvents()
                        .stream()
                        .map(e -> new GetPaymentRes.Event(e.getType().name(), e.getAttemptedOn(), e.getError()))
                        .collect(Collectors.toList())
        );
    }

}
