package chainizer.payment.eth.payment.query;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.List;

public class GetPaymentRes {

    private final String address;
    private final Instant createdOn;
    private final String callbackUrl;
    private final String balance;
    private final List<Event> events;

    public GetPaymentRes(String address, Instant createdOn, String callbackUrl, String balance, List<Event> events) {
        this.address = address;
        this.createdOn = createdOn;
        this.callbackUrl = callbackUrl;
        this.balance = balance;
        this.events = events;
    }

    public String getAddress() {
        return address;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getBalance() {
        return balance;
    }

    public List<Event> getEvents() {
        return events;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("createdOn", createdOn)
                .append("url", callbackUrl)
                .append("balance", balance)
                .append("events", events)
                .toString();
    }

    public static class Event {
        private final String type;
        private final Instant attemptedOn;
        private final String error;

        public Event(String type, Instant attemptedOn, String error) {
            this.type = type;
            this.attemptedOn = attemptedOn;
            this.error = error;
        }

        public String getType() {
            return type;
        }

        public Instant getAttemptedOn() {
            return attemptedOn;
        }

        public String getError() {
            return error;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .appendSuper(super.toString())
                    .append("type", type)
                    .append("attemptedOn", attemptedOn)
                    .append("error", error)
                    .toString();
        }
    }

}
