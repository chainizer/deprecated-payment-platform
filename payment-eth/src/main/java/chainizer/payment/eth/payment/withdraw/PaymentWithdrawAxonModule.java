package chainizer.payment.eth.payment.withdraw;

import chainizer.support.axon.AxonModule;
import org.axonframework.config.Configurer;

import javax.inject.Inject;

public class PaymentWithdrawAxonModule implements AxonModule {

    @Inject
    private PaymentWithdrawHandler paymentWithdrawHandler;

    @Override
    public void configure(Configurer configurer) {
        configurer.registerCommandHandler(configuration -> paymentWithdrawHandler);
    }

}
