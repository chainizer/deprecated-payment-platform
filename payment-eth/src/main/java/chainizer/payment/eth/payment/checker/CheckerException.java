package chainizer.payment.eth.payment.checker;

import chainizer.support.core.CodifiedException;
import chainizer.support.core.CodifiedMessage;

class CheckerException extends CodifiedException {

    CheckerException(CodifiedMessage codifiedMessage, Object... params) {
        super(codifiedMessage, params);
    }

    CheckerException(Throwable cause, CodifiedMessage codifiedMessage, Object... params) {
        super(cause, codifiedMessage, params);
    }

}
