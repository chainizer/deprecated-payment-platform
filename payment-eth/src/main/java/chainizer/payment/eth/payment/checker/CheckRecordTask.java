package chainizer.payment.eth.payment.checker;

import chainizer.payment.eth.parity.Executor;
import chainizer.payment.eth.payment.LogCallResultCmd;
import chainizer.payment.eth.payment.PaymentErrorMsg;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.support.axon.CommandGateway;
import chainizer.support.core.CodifiedException;
import chainizer.support.core.TechnicalErrorMsg;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.parity.Parity;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

public class CheckRecordTask implements Runnable {

    private static final int DEFAULT_CALLBACK_TIMEOUT = 2;

    private final static Logger LOGGER = LoggerFactory.getLogger(CheckRecordTask.class);

    private final Configuration configuration;

    private final Parity parity;

    private final CommandGateway commandGateway;

    private final Record record;

    public CheckRecordTask(Configuration configuration, Parity parity, CommandGateway commandGateway, Record record) {
        this.configuration = configuration;
        this.parity = parity;
        this.commandGateway = commandGateway;
        this.record = record;
    }

    private void dispatchResult(Record record, CodifiedException exception) {
        commandGateway.send(new LogCallResultCmd(
                record.getAddress(),
                exception != null ? exception.getMessage() : null
        ));
        if (exception != null) {
            LOGGER.debug("notification failed", exception);
        }
    }

    private void notifyPayment(Record record) {
        LOGGER.debug("notifyPayment");

        String callbackUrl = record.getCallbackUrl();

        String url = callbackUrl.contains("?")
                ? String.format("%s&address=%s", callbackUrl, record.getAddress())
                : String.format("%s?address=%s", callbackUrl, record.getAddress());

        Request request;
        try {
            request = new Request.Builder().url(url).get().build();
        } catch (IllegalArgumentException e) {
            dispatchResult(record, new CheckerException(e, PaymentErrorMsg.BAD_CALLBACK_URL, callbackUrl));
            return;
        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        OkHttpClient client = builder.build();
        builder.connectTimeout(
                configuration.getInt("chainizer.payment.eth.checker.callback.timeout.connect", DEFAULT_CALLBACK_TIMEOUT),
                TimeUnit.SECONDS
        );
        builder.readTimeout(
                configuration.getInt("chainizer.payment.eth.checker.callback.timeout.read", DEFAULT_CALLBACK_TIMEOUT),
                TimeUnit.SECONDS
        );

        try {
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                dispatchResult(record, null);
            } else {
                dispatchResult(record, new CheckerException(PaymentErrorMsg.BAD_CALLBACK_HTTP_RESPONSE, response.code()));
            }

        } catch (IOException e) {
            dispatchResult(record, new CheckerException(e, PaymentErrorMsg.CALLBACK_CONNECTION_FAILED));
        }
    }

    @Override
    public void run() {
        try {
            LOGGER.debug("check record {}", record);

            BigInteger balance = Executor.call(
                    () -> parity.ethGetBalance(record.getAddress(), DefaultBlockParameterName.LATEST).send()
            ).getBalance();

            if (balance.compareTo(BigInteger.ZERO) > 0) {
                notifyPayment(record);
            }

        } catch (CodifiedException e) {
            dispatchResult(record, e);
        } catch (Exception e) {
            dispatchResult(record, new CheckerException(e, TechnicalErrorMsg.UNEXPECTED_EXCEPTION));
        }
    }

}
