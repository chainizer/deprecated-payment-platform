package chainizer.payment.eth.payment.checker;

import chainizer.support.axon.AxonModule;
import org.axonframework.config.Configurer;

import javax.inject.Inject;

public class PaymentCheckerAxonModule implements AxonModule {

    @Inject
    private PaymentCheckerHandler paymentCheckerHandler;

    @Override
    public void configure(Configurer configurer) {
        configurer.registerCommandHandler(configuration -> paymentCheckerHandler);
    }

}
