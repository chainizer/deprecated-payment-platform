package chainizer.payment.eth.payment;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;

public class PaymentGeneratedEvt {

    private final String address;

    private final String encryptedPrivateKey;

    private final String challenge;

    private final String callbackUrl;

    private final Instant createdOn;

    public PaymentGeneratedEvt(String address, String encryptedPrivateKey, String challenge, String callbackUrl, Instant createdOn) {
        this.address = address;
        this.encryptedPrivateKey = encryptedPrivateKey;
        this.challenge = challenge;
        this.callbackUrl = callbackUrl;
        this.createdOn = createdOn;
    }

    public String getAddress() {
        return address;
    }

    public String getEncryptedPrivateKey() {
        return encryptedPrivateKey;
    }

    public String getChallenge() {
        return challenge;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("encryptedPrivateKey", encryptedPrivateKey)
                .append("challenge", challenge)
                .append("callbackUrl", callbackUrl)
                .append("createdOn", createdOn)
                .toString();
    }
}
