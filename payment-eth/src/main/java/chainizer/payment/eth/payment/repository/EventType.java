package chainizer.payment.eth.payment.repository;

public enum EventType {
    notification, withdraw
}
