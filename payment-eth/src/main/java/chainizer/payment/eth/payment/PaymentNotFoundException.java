package chainizer.payment.eth.payment;

import chainizer.support.core.CodifiedException;

public class PaymentNotFoundException extends CodifiedException {

    public PaymentNotFoundException(String address) {
        super(PaymentErrorMsg.PAYMENT_NOT_FOUND, address);
    }

}
