package chainizer.payment.eth.payment;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;

public class WithdrawSucceedEvt {

    private final String address;

    private final Instant succeedOn;

    public WithdrawSucceedEvt(String address, Instant succeedOn) {
        this.address = address;
        this.succeedOn = succeedOn;
    }

    public String getAddress() {
        return address;
    }

    public Instant getSucceedOn() {
        return succeedOn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("succeedOn", succeedOn)
                .toString();
    }

}
