package chainizer.payment.eth.payment;

import chainizer.support.core.CodifiedException;

import java.math.BigInteger;

public class NotEnoughFundException extends CodifiedException {

    public NotEnoughFundException(BigInteger balance, BigInteger minimum) {
        super(PaymentErrorMsg.NOT_ENOUGH_FUND, balance, minimum);
    }

}
