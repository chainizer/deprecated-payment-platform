package chainizer.payment.eth.payment;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.axonframework.commandhandling.TargetAggregateIdentifier;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

public class GeneratePaymentCmd {

    @NotBlank
    @TargetAggregateIdentifier
    private final String address;

    @NotBlank
    private final String encryptedPrivateKey;

    @NotBlank
    private final String challenge;

    @NotBlank
    @URL
    private final String callbackUrl;

    public GeneratePaymentCmd(@NotBlank String address, @NotBlank String encryptedPrivateKey, @NotBlank String challenge, @NotBlank @URL String callbackUrl) {
        this.address = address;
        this.encryptedPrivateKey = encryptedPrivateKey;
        this.challenge = challenge;
        this.callbackUrl = callbackUrl;
    }

    public String getAddress() {
        return address;
    }

    public String getEncryptedPrivateKey() {
        return encryptedPrivateKey;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getChallenge() {
        return challenge;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("challenge", challenge)
                .append("encryptedPrivateKey", encryptedPrivateKey)
                .append("callbackUrl", callbackUrl)
                .toString();
    }

}
