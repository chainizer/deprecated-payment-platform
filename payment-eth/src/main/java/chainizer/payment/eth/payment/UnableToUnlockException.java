package chainizer.payment.eth.payment;

import chainizer.support.core.CodifiedException;

public class UnableToUnlockException extends CodifiedException {

    public UnableToUnlockException(String address) {
        super(PaymentErrorMsg.UNABLE_TO_UNLOCK, address);
    }

}
