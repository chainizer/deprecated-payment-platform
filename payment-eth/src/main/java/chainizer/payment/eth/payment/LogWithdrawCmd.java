package chainizer.payment.eth.payment;

import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmdRes;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public class LogWithdrawCmd {

    @TargetAggregateIdentifier
    @NotBlank
    private final String address;

    @NotBlank
    private final String toAddress;

    private final String message;

    private final String feeHash;

    private final String userHash;

    @NotNull
    private final Instant attemptedOn;

    private final boolean successful;

    private final String error;

    public LogWithdrawCmd(String address, String toAddress, String message, WithdrawPaymentCmdRes res) {
        this.address = address;
        this.toAddress = toAddress;
        this.message = message;
        this.feeHash = res.getFeeHash();
        this.userHash = res.getUserHash();
        this.attemptedOn = res.getAttemptedOn();
        this.successful = res.isSuccessful();
        this.error = res.getError();
    }

    public LogWithdrawCmd(String address, String toAddress, String message, String feeHash, String userHash) {
        this.address = address;
        this.toAddress = toAddress;
        this.message = message;
        this.feeHash = feeHash;
        this.userHash = userHash;
        this.attemptedOn = Instant.now();
        this.successful = true;
        this.error = null;
    }

    public LogWithdrawCmd(String address, String toAddress, String message, String error) {
        this.address = address;
        this.toAddress = toAddress;
        this.message = message;
        this.feeHash = null;
        this.userHash = null;
        this.attemptedOn = Instant.now();
        this.successful = false;
        this.error = error;
    }

    public String getAddress() {
        return address;
    }

    public String getToAddress() {
        return toAddress;
    }

    public String getMessage() {
        return message;
    }

    public String getFeeHash() {
        return feeHash;
    }

    public String getUserHash() {
        return userHash;
    }

    public Instant getAttemptedOn() {
        return attemptedOn;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getError() {
        return error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("toAddress", toAddress)
                .append("message", message)
                .append("feeHash", feeHash)
                .append("userHash", userHash)
                .append("attemptedOn", attemptedOn)
                .append("successful", successful)
                .append("error", error)
                .toString();
    }
}

