package chainizer.payment.eth.payment.checker;

import org.axonframework.commandhandling.CommandHandler;

import javax.inject.Inject;

public class PaymentCheckerHandler {

    private final CheckRecordsTask checkRecordsTask;

    @Inject
    public PaymentCheckerHandler(CheckRecordsTask checkRecordsTask) {
        this.checkRecordsTask = checkRecordsTask;
    }

    @CommandHandler
    @SuppressWarnings("UnusedDeclaration")
    public boolean handle(CheckPaymentsCmd cmd) {
        return checkRecordsTask.call();
    }
}
