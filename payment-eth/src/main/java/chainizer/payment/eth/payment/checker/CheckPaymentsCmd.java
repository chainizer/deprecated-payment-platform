package chainizer.payment.eth.payment.checker;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CheckPaymentsCmd {

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .toString();
    }

}
