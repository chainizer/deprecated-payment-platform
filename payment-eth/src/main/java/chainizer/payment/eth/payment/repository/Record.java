package chainizer.payment.eth.payment.repository;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Record {

    private String address;

    private String encryptedPrivateKey;

    private String challenge;

    private String callbackUrl;

    private Instant createdOn;

    private boolean notified;

    private Instant lastNotificationAttemptedOn;

    private Instant lastWithdrawAttemptedOn;

    private boolean withdrawn;

    private List<Event> events;

    public Record() {
        this.events = new ArrayList<>();
    }

    public Record(String address, String encryptedPrivateKey, String challenge, String callbackUrl, Instant createdOn) {
        this.address = address;
        this.encryptedPrivateKey = encryptedPrivateKey;
        this.challenge = challenge;
        this.callbackUrl = callbackUrl;
        this.createdOn = createdOn;
        this.events = new ArrayList<>();
    }

    public Record(String address, String encryptedPrivateKey, String challenge, String callbackUrl, Instant createdOn, boolean notified, Instant lastNotificationAttemptedOn, boolean withdrawn, List<Event> events) {
        this.address = address;
        this.encryptedPrivateKey = encryptedPrivateKey;
        this.challenge = challenge;
        this.callbackUrl = callbackUrl;
        this.createdOn = createdOn;
        this.notified = notified;
        this.lastNotificationAttemptedOn = lastNotificationAttemptedOn;
        this.withdrawn = withdrawn;
        this.events = events;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEncryptedPrivateKey() {
        return encryptedPrivateKey;
    }

    public void setEncryptedPrivateKey(String encryptedPrivateKey) {
        this.encryptedPrivateKey = encryptedPrivateKey;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public Instant getLastNotificationAttemptedOn() {
        return lastNotificationAttemptedOn;
    }

    public void setLastNotificationAttemptedOn(Instant lastNotificationAttemptedOn) {
        this.lastNotificationAttemptedOn = lastNotificationAttemptedOn;
    }

    public Instant getLastWithdrawAttemptedOn() {
        return lastWithdrawAttemptedOn;
    }

    public void setLastWithdrawAttemptedOn(Instant lastWithdrawAttemptedOn) {
        this.lastWithdrawAttemptedOn = lastWithdrawAttemptedOn;
    }

    public boolean isWithdrawn() {
        return withdrawn;
    }

    public void setWithdrawn(boolean withdrawn) {
        this.withdrawn = withdrawn;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        return new EqualsBuilder()
                .append(address, record.address)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(address)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("encryptedPrivateKey", encryptedPrivateKey)
                .append("challenge", challenge)
                .append("callbackUrl", callbackUrl)
                .append("createdOn", createdOn)
                .append("notified", notified)
                .append("lastNotificationAttemptedOn", lastNotificationAttemptedOn)
                .append("lastWithdrawAttemptedOn", lastWithdrawAttemptedOn)
                .append("withdrawn", withdrawn)
                .append("events", events)
                .toString();
    }
}
