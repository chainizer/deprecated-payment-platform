package chainizer.payment.eth.payment;

import chainizer.support.axon.AxonModule;
import org.axonframework.config.Configurer;

public class PaymentAxonModule implements AxonModule {

    @Override
    public void configure(Configurer configurer) {
        configurer.configureAggregate(Payment.class);
    }

}
