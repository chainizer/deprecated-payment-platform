package chainizer.payment.eth.payment.withdraw;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

public class WithdrawPaymentCmd {

    @NotNull
    private final String address;

    @NotNull
    private final String toAddress;

    private final String message;

    public WithdrawPaymentCmd(String address, String toAddress, String message) {
        this.address = address;
        this.toAddress = toAddress;
        this.message = message;
    }

    public String getAddress() {
        return address;
    }

    public String getToAddress() {
        return toAddress;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("toAddress", toAddress)
                .append("message", message)
                .toString();
    }

}
