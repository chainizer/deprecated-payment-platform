package chainizer.payment.eth.payment;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public class LogCallResultCmd {

    @TargetAggregateIdentifier
    @NotBlank
    private final String address;

    @NotNull
    private final Instant attemptedOn;

    private final boolean successful;

    private final String error;

    public LogCallResultCmd(String address, String error) {
        this.address = address;
        this.attemptedOn = Instant.now();
        if (StringUtils.isBlank(error)) {
            this.successful = true;
            this.error = null;
        } else {
            this.successful = false;
            this.error = error;
        }
    }

    public String getAddress() {
        return address;
    }

    public Instant getAttemptedOn() {
        return attemptedOn;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getError() {
        return error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("address", address)
                .append("calledOn", attemptedOn)
                .append("successful", successful)
                .append("error", error)
                .toString();
    }
}
