package chainizer.payment.eth.payment.checker;

import chainizer.payment.eth.payment.BaseTest;
import chainizer.payment.eth.payment.LogCallResultCmd;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.axon.CommandGateway;
import com.mongodb.Block;
import org.apache.commons.configuration2.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.mockito.internal.verification.Times;
import org.web3j.protocol.parity.Parity;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verify;

public class CheckRecordsTaskTest extends BaseTest {

    @Spy
    private Configuration configuration = TestUtils.resolveConfiguration();

    @Spy
    private Parity parity = TestUtils.createParity(configuration);

    @Mock
    private CommandGateway commandGateway;

    @Mock
    private Repository repository;

    @InjectMocks
    private CheckRecordsTask checkRecordsTask;

    @Before
    public void setUp() {
        clearInvocations(configuration);
        clearInvocations(parity);
        clearInvocations(commandGateway);
        clearInvocations(repository);
    }

    @Test
    public void shouldNotified() {
        Record record = fd.record(0);
        sendTo(parity, record.getAddress());

        Mockito.doAnswer(invocation -> {
            Block<Record> block = invocation.getArgument(2);
            block.apply(record);
            return null;
        }).when(repository).listActiveRecords(any(), any(), any());

        assertTrue(checkRecordsTask.call());

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        null
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldHandleRuntimeException() {
        Record record = fd.record(0);
        sendTo(parity, record.getAddress());

        Mockito.doThrow(new RuntimeException()).when(repository).listActiveRecords(any(), any(), any());

        assertFalse(checkRecordsTask.call());

        verify(commandGateway, new Times(0)).send(any());
    }

    @Test
    public void shouldHandleInterruptedException() {
        Record record = fd.record(0);
        sendTo(parity, record.getAddress());

        Mockito.doAnswer(invocation -> {
            Block<Record> block = invocation.getArgument(2);
            block.apply(record);
            return null;
        }).when(repository).listActiveRecords(any(), any(), any());

        AtomicBoolean result = new AtomicBoolean(false);
        Thread thread = new Thread(() -> result.set(checkRecordsTask.call()));
        thread.start();
        thread.interrupt();
        assertFalse(result.get());

        verify(commandGateway, new Times(0)).send(any());
    }

}
