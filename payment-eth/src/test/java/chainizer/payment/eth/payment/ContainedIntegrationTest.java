package chainizer.payment.eth.payment;

import chainizer.payment.eth.payment.checker.CheckPaymentsCmd;
import chainizer.payment.eth.payment.query.GetPaymentReq;
import chainizer.payment.eth.payment.query.GetPaymentRes;
import chainizer.payment.eth.payment.repository.EventType;
import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmd;
import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmdRes;
import chainizer.support.axon.AxonMetaData;
import chainizer.support.axon.CommandGateway;
import chainizer.support.axon.QueryGateway;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.junit.Test;
import org.web3j.protocol.parity.Parity;

import static org.junit.Assert.*;

public class ContainedIntegrationTest extends BaseContainedTest {

    @Test
    public void shouldGenerateCheckWithdraw() {
        CommandGateway commandGateway = container.get(CommandGateway.class);
        QueryGateway queryGateway = container.get(QueryGateway.class);
        Parity parity = container.get(Parity.class);

        commandGateway.sendAndWait(fd.createGeneratePaymentCmd(0));

        sendTo(parity, fd.payment(0).address);

        commandGateway.sendAndWait(new CheckPaymentsCmd());

        GetPaymentRes getPaymentRes = queryGateway.sendAndWait(
                new GetPaymentReq(fd.payment(0).address),
                GetPaymentRes.class,
                fd.payment(0).token
        );

        assertNotNull(getPaymentRes);
        assertEquals(getPaymentRes.getBalance(), "2000000000000000000");
        assertNotNull(getPaymentRes.getEvents());
        assertEquals(getPaymentRes.getEvents().size(), 1);
        assertEquals(getPaymentRes.getEvents().get(0).getType(), EventType.notification.name());
        assertNull(getPaymentRes.getEvents().get(0).getError());
        assertNotNull(getPaymentRes.getEvents().get(0).getAttemptedOn());

        CommandMessage<WithdrawPaymentCmd> withdrawPaymentCmdCommandMessage = new GenericCommandMessage<>(
                fd.createWithdrawPaymentCmd(0, 1),
                AxonMetaData.empty().put("token", fd.payment(0).token).get()
        );
        WithdrawPaymentCmdRes withdrawPaymentCmdRes = commandGateway.sendAndWait(withdrawPaymentCmdCommandMessage);

        assertNull(withdrawPaymentCmdRes.getError());
        assertNotNull(withdrawPaymentCmdRes.getAttemptedOn());
        assertNotNull(withdrawPaymentCmdRes.getFeeHash());
        assertNotNull(withdrawPaymentCmdRes.getUserHash());
    }

}
