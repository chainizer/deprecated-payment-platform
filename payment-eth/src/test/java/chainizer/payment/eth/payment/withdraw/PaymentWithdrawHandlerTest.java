package chainizer.payment.eth.payment.withdraw;

import chainizer.payment.eth.payment.BaseTest;
import chainizer.payment.eth.payment.LogWithdrawCmd;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.axon.CommandGateway;
import org.apache.commons.configuration2.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.verification.Times;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PaymentWithdrawHandlerTest extends BaseTest {

    @Spy
    private Configuration configuration = TestUtils.resolveConfiguration();

    @Mock
    private Repository repository;

    @Mock
    private CommandGateway commandGateway;

    @Spy
    private Parity parity = TestUtils.createParity(configuration);

    @InjectMocks
    private PaymentWithdrawHandler paymentWithdrawHandler;

    @Before
    public void setUp() {
        clearInvocations(configuration);
        clearInvocations(repository);
        clearInvocations(commandGateway);
        clearInvocations(parity);
    }

    @Test
    public void shouldWithdrawWithoutFee() throws Exception {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(this.fd.record(0));

        sendTo(parity, fd.payment(0).address, "0.0009");

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                fd.payment(0).address,
                fd.payment(1).address,
                "a message"
        ), fd.payment(0).token);

        assertTrue(res.isSuccessful());
        assertNull(res.getError());
        assertNull(res.getFeeHash());
        assertNotNull(res.getUserHash());
        assertNotNull(res.getAttemptedOn());

        String withdrawnAmount = parity.ethGetTransactionByHash(res.getUserHash()).send().getTransaction().get().getValue().toString();
        assertEquals("885785656414000", withdrawnAmount);
    }

    @Test
    public void shouldWithdrawWithAllTaxedBalance() throws Exception {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(this.fd.record(0));

        sendTo(parity, fd.payment(0).address, "3");

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                fd.payment(0).address,
                fd.payment(1).address,
                "a message"
        ), fd.payment(0).token);

        assertTrue(res.isSuccessful());
        assertNull(res.getError());
        assertNotNull(res.getFeeHash());
        assertNotNull(res.getUserHash());
        assertNotNull(res.getAttemptedOn());

        String feeAmount = parity.ethGetTransactionByHash(res.getFeeHash()).send().getTransaction().get().getValue().toString();
        assertEquals("90000000000000000", feeAmount);

        String withdrawnAmount = parity.ethGetTransactionByHash(res.getUserHash()).send().getTransaction().get().getValue().toString();
        assertEquals("2909985785656414000", withdrawnAmount);
    }

    @Test
    public void shouldWithdrawWithMaxTaxableBalance() throws Exception {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(this.fd.record(0));

        sendTo(parity, fd.payment(0).address, "61");

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                fd.payment(0).address,
                fd.payment(1).address,
                "a message"
        ), fd.payment(0).token);

        assertTrue(res.isSuccessful());
        assertNull(res.getError());
        assertNotNull(res.getFeeHash());
        assertNotNull(res.getUserHash());
        assertNotNull(res.getAttemptedOn());

        String feeAmount = parity.ethGetTransactionByHash(res.getFeeHash()).send().getTransaction().get().getValue().toString();
        assertEquals("1800000000000000000", feeAmount);

        String withdrawnAmount = parity.ethGetTransactionByHash(res.getUserHash()).send().getTransaction().get().getValue().toString();
        assertEquals("59199985785656414000", withdrawnAmount);
    }

    @Test
    public void shouldFailedWhenBadParityEndpoint() {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(this.fd.record(0));

        Parity parity = Parity.build(new HttpService("https://localhost:8558"));

        PaymentWithdrawHandler paymentWithdrawHandler = new PaymentWithdrawHandler(
                configuration,
                repository,
                commandGateway,
                parity
        );

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                fd.payment(0).address,
                fd.payment(1).address,
                "a message"
        ), fd.payment(0).token);

        assertFalse(res.isSuccessful());
        assertEquals(res.getError(), "PARITY_REMOTE_CALL_FAILED - The remote call failed.");

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogWithdrawCmd(
                        fd.payment(0).address,
                        fd.payment(1).address,
                        "a message",
                        res
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldFailedWhenNotEnoughFund() {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(this.fd.record(0));

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                this.fd.payment(3).address,
                this.fd.payment(4).address,
                "a message"
        ), fd.payment(3).token);

        assertFalse(res.isSuccessful());
        assertEquals(res.getError(), "NOT_ENOUGH_FUND - The balance is not enough: [0], [14214343586000] are missing.");

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogWithdrawCmd(
                        fd.payment(3).address,
                        fd.payment(4).address,
                        "a message",
                        res
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldReturnNullWhenNotFound() {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(null);

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                this.fd.payment(3).address,
                this.fd.payment(4).address,
                "a message"
        ), fd.payment(3).token);

        assertNull(res);

        verify(commandGateway, new Times(0)).send(any());
    }

    @Test
    public void shouldFailedWhenRuntimeException() {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(fd.record(3));
        when(parity.ethCoinbase()).thenThrow(NullPointerException.class);

        WithdrawPaymentCmdRes res = paymentWithdrawHandler.handle(new WithdrawPaymentCmd(
                this.fd.payment(3).address,
                this.fd.payment(4).address,
                "a message"
        ), fd.payment(3).token);

        assertFalse(res.isSuccessful());
        assertEquals(res.getError(), "UNEXPECTED_EXCEPTION - An unexpected exception occurred.");

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogWithdrawCmd(
                        fd.payment(3).address,
                        fd.payment(4).address,
                        "a message",
                        res
                ),
                "attemptedOn"
        ));
    }
}
