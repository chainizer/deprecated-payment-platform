package chainizer.payment.eth.payment.repository;

import chainizer.payment.eth.payment.BaseTest;
import chainizer.payment.eth.support.Fixtures;
import chainizer.payment.eth.support.Log;
import chainizer.payment.eth.support.TestUtils;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class RepositoryTest extends BaseTest {

    private Repository repository;

    @Before
    public void setUp() {
        repository = new Repository(TestUtils.resolveConfiguration());
    }

    @Test
    public void shouldSaveAndLoad() {
        Fixtures.Payment p = fd.payment(0);

        Record record = new Record(
                p.address,
                p.encryptedPrivateKey,
                p.challenge,
                p.callbackUrl,
                p.createdOn
        );

        repository.save(record);

        Event event1 = new Event(
                EventType.notification,
                Instant.now(),
                null
        );
        repository.addEvent(p.address, event1);

        Event event2 = new Event(
                EventType.notification,
                Instant.now(),
                "error"
        );
        repository.addEvent(p.address, event2);

        Record loadedRecord = repository.getByAddress(p.address);
        assertNotNull(loadedRecord);

        assertEquals(loadedRecord.getAddress(), p.address);
        assertEquals(loadedRecord.getEncryptedPrivateKey(), p.encryptedPrivateKey);
        assertEquals(loadedRecord.getChallenge(), p.challenge);
        assertEquals(loadedRecord.getCallbackUrl(), p.callbackUrl);
        assertEquals(loadedRecord.getCreatedOn(), p.createdOn);
        assertEquals(loadedRecord.getEvents().size(), 2);

        assertEquals(loadedRecord.getEvents().get(0).getAttemptedOn(), event1.getAttemptedOn());
        assertEquals(loadedRecord.getEvents().get(0).isSuccessful(), event1.isSuccessful());
        assertEquals(loadedRecord.getEvents().get(0).getError(), event1.getError());

        assertEquals(loadedRecord.getEvents().get(1).getAttemptedOn(), event2.getAttemptedOn());
        assertEquals(loadedRecord.getEvents().get(1).isSuccessful(), event2.isSuccessful());
        assertEquals(loadedRecord.getEvents().get(1).getError(), event2.getError());

        Record loadedRecordWithChallenge = repository.getByAddress(p.address, p.challenge);
        assertNotNull(loadedRecordWithChallenge);

        Record loadedRecordWithChallengeBis = repository.getByAddress(p.address, "unknown");
        assertNull(loadedRecordWithChallengeBis);
    }

    @Test
    public void shouldNotGetByAddress() {
        Record loadedRecord = repository.getByAddress("unknown");
        assertNull(loadedRecord);
    }


    @Test
    public void shouldFindActiveRecords() {
        // valid because created early and not yet attempted

        Fixtures.Payment p1 = fd.payment(1);
        p1.createdOn = Instant.now().minus(1, ChronoUnit.DAYS);
        p1.lastNotificationAttemptedOn = null;
        repository.save(new Record(
                p1.address,
                p1.encryptedPrivateKey,
                p1.challenge,
                p1.callbackUrl,
                p1.createdOn,
                false,
                null,
                false,
                Collections.emptyList()
        ));

        // valid because created early and can be attempted
        Fixtures.Payment p2 = fd.payment(2);
        p2.createdOn = Instant.now().minus(1, ChronoUnit.DAYS);
        p2.lastNotificationAttemptedOn = Instant.now().minus(45, ChronoUnit.MINUTES);
        repository.save(new Record(
                p2.address,
                p2.encryptedPrivateKey,
                p2.challenge,
                p2.callbackUrl,
                p2.createdOn,
                false,
                p2.lastNotificationAttemptedOn,
                false,
                Collections.emptyList()
        ));

        // not valid because created early but already attempted
        Fixtures.Payment p3 = fd.payment(3);
        p3.createdOn = Instant.now().minus(1, ChronoUnit.DAYS);
        p3.lastNotificationAttemptedOn = Instant.now().minus(15, ChronoUnit.MINUTES);
        repository.save(new Record(
                p3.address,
                p3.encryptedPrivateKey,
                p3.challenge,
                p3.callbackUrl,
                p3.createdOn,
                false,
                p3.lastNotificationAttemptedOn,
                false,
                Collections.emptyList()
        ));

        // not valid because created early but already notified
        Fixtures.Payment p4 = fd.payment(4);
        p4.createdOn = Instant.now().minus(1, ChronoUnit.DAYS);
        p4.lastNotificationAttemptedOn = Instant.now().minus(45, ChronoUnit.MINUTES);
        repository.save(new Record(
                p4.address,
                p4.encryptedPrivateKey,
                p1.challenge,
                p4.callbackUrl,
                p4.createdOn,
                true,
                p4.lastNotificationAttemptedOn,
                false,
                Collections.emptyList()
        ));

        // not valid because created early but already withdrawn
        Fixtures.Payment p5 = fd.payment(5);
        p5.createdOn = Instant.now().minus(1, ChronoUnit.DAYS);
        p5.lastNotificationAttemptedOn = null;
        repository.save(new Record(
                p5.address,
                p5.encryptedPrivateKey,
                p5.challenge,
                p5.callbackUrl,
                p5.createdOn,
                false,
                null,
                true,
                Collections.emptyList()
        ));

        // not valid because too old
        Fixtures.Payment p6 = fd.payment(6);
        p6.createdOn = Instant.now().minus(5, ChronoUnit.DAYS);
        p6.lastNotificationAttemptedOn = null;
        repository.save(new Record(
                p6.address,
                p6.encryptedPrivateKey,
                p6.challenge,
                p6.callbackUrl,
                p6.createdOn,
                false,
                null,
                false,
                Collections.emptyList()
        ));

        Instant minCreatedOn = Instant.now().minus(2, ChronoUnit.DAYS);
        Instant maxNotifiedOn = Instant.now().minus(30, ChronoUnit.MINUTES);

        checkRecord(minCreatedOn, maxNotifiedOn, p1.address);
        checkRecord(minCreatedOn, maxNotifiedOn, p2.address);
        checkRecord(minCreatedOn, maxNotifiedOn, p3.address);
        checkRecord(minCreatedOn, maxNotifiedOn, p4.address);
        checkRecord(minCreatedOn, maxNotifiedOn, p5.address);
        checkRecord(minCreatedOn, maxNotifiedOn, p6.address);

        List<Record> records = new ArrayList<>();
        repository.listActiveRecords(minCreatedOn, maxNotifiedOn, record -> {
            Log.debug("loaded {}", record);
            records.add(record);
        });

        assertEquals(records.size(), 2);

    }

    private void checkRecord(Instant minCreatedOn, Instant maxNotifiedOn, String address) {
        Document d = repository.getPayments().find(Filters.eq("address", address)).first();

        Log.debug(
                "{} - {} {} {} {}",
                address,
                !d.getBoolean("notified"),
                !d.getBoolean("withdrawn"),
                d.getLong("createdOn") > minCreatedOn.toEpochMilli(),
                d.getLong("lastNotificationAttemptedOn") == null || d.getLong("lastNotificationAttemptedOn") < maxNotifiedOn.toEpochMilli()
        );
    }

}
