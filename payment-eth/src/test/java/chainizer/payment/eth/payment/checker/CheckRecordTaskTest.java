package chainizer.payment.eth.payment.checker;

import chainizer.payment.eth.payment.BaseTest;
import chainizer.payment.eth.payment.LogCallResultCmd;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.axon.CommandGateway;
import org.apache.commons.configuration2.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.verification.Times;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verify;

public class CheckRecordTaskTest extends BaseTest {

    @Spy
    private Configuration configuration = TestUtils.resolveConfiguration();

    @Spy
    private Parity parity = TestUtils.createParity(configuration);

    @Mock
    private CommandGateway commandGateway;

    @Before
    public void setUp() {
        clearInvocations(configuration);
        clearInvocations(parity);
        clearInvocations(commandGateway);
    }

    @Test
    public void shouldNotified() {
        Record record = fd.record(0);
        sendTo(parity, record.getAddress());

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        null
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldNotifiedWhenUrlWithQuery() {
        Record record = fd.record(0);
        record.setCallbackUrl("http://www.google.com?test=test");
        sendTo(parity, record.getAddress());

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        null
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldFailedWhenBadParityEndpoint() {
        Record record = fd.record(0);
        sendTo(parity, record.getAddress());

        Parity parity = Parity.build(new HttpService("https://localhost:8558"));

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        "UNEXPECTED_EXCEPTION - An unexpected exception occurred."
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldFailedWhenBadAddress() {
        Record record = fd.record(0);
        record.setCallbackUrl("www.google.com?test=test");
        sendTo(parity, record.getAddress());

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        "BAD_CALLBACK_URL - The URL is not valid: [www.google.com?test=test]."
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldSkipWhenHttp404() {
        Record record = fd.record(0);
        record.setCallbackUrl("http://www.google.com/404");
        sendTo(parity, record.getAddress());

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        "BAD_CALLBACK_HTTP_RESPONSE - The HTTP status is not valid: [404]."
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldSkipWhenHttpConnectionFailed() {
        Record record = fd.record(0);
        record.setCallbackUrl("http://localhost");
        sendTo(parity, record.getAddress());

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(1)).send(ArgumentMatchers.refEq(
                new LogCallResultCmd(
                        fd.payment(0).address,
                        "CALLBACK_CONNECTION_FAILED - The HTTP connection failed."
                ),
                "attemptedOn"
        ));
    }

    @Test
    public void shouldSkipWhenBalanceIs0() {
        Record record = fd.record(1);

        CheckRecordTask checkRecordTask = new CheckRecordTask(
                configuration,
                parity,
                commandGateway,
                record
        );

        checkRecordTask.run();

        verify(commandGateway, new Times(0)).send(any());
    }

}
