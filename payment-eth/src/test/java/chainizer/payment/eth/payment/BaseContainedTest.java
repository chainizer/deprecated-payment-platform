package chainizer.payment.eth.payment;

import chainizer.payment.eth.support.TestUtils;
import chainizer.support.container.Container;
import chainizer.support.core.ConfigurationBinder;
import chainizer.support.mongo.Mongo;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.After;
import org.junit.Before;

public abstract class BaseContainedTest extends BaseTest {

    protected Container container;

    protected ServiceLocator serviceLocator;

    @Before
    public void startContainer() {
        serviceLocator = ServiceLocatorFactory.getInstance().create(BaseContainedTest.class.getName());
        container = new Container();
        container.configure(
                serviceLocator::getService,
                serviceLocator::inject,
                binder -> ServiceLocatorUtilities.bind(serviceLocator, binder),
                new ConfigurationBinder(TestUtils.resolveConfiguration())
        );
        container.start();
    }

    @After
    public void stopContainer() {
        container.stop();
        serviceLocator.shutdown();
        Mongo.close();
    }

}
