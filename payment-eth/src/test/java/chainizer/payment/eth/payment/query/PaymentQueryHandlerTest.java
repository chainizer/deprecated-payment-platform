package chainizer.payment.eth.payment.query;

import chainizer.payment.eth.payment.*;
import chainizer.payment.eth.payment.repository.Event;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.payment.repository.Repository;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.core.CodifiedException;
import org.apache.commons.configuration2.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.web3j.protocol.parity.Parity;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PaymentQueryHandlerTest extends BaseTest {

    @Spy
    private Configuration configuration = TestUtils.resolveConfiguration();

    @Mock
    private Repository repository;

    @Spy
    private Parity parity = TestUtils.createParity(configuration);

    @InjectMocks
    private PaymentQueryHandler paymentQueryHandler;

    @Before
    public void setUp() {
        clearInvocations(repository);
    }

    @Test
    public void shouldSaveWhenPaymentGeneratedEvt() {
        PaymentGeneratedEvt evt = fd.createPaymentGeneratedEvt(0);
        paymentQueryHandler.on(evt);
        verify(repository).save(any(Record.class));
    }

    @Test
    public void shouldAddWhenNotificationFailedEvt() {
        NotificationFailedEvt evt = fd.createNotificationFailedEvt(0, "error");
        paymentQueryHandler.on(evt);
        verify(repository).addEvent(anyString(), any(Event.class));
    }

    @Test
    public void shouldAddWhenNotificationSucceedEvt() {
        NotificationSucceedEvt evt = fd.createNotificationSucceedEvt(0);
        paymentQueryHandler.on(evt);
        verify(repository).addEvent(anyString(), any(Event.class));
    }

    @Test
    public void shouldAddWhenWithdrawFailedEvt() {
        WithdrawFailedEvt evt = fd.createWithdrawFailedEvt(0, "error");
        paymentQueryHandler.on(evt);
        verify(repository).addEvent(anyString(), any(Event.class));
    }

    @Test
    public void shouldAddWhenWithdrawSucceedEvt() {
        WithdrawSucceedEvt evt = fd.createWithdrawSucceedEvt(0);
        paymentQueryHandler.on(evt);
        verify(repository).addEvent(anyString(), any(Event.class));
    }

    @Test
    public void shouldFailedWhenNotFound() throws CodifiedException {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(null);
        GetPaymentRes getPaymentRes = paymentQueryHandler.handle(
                new GetPaymentReq(fd.payment(0).address),
                fd.payment(0).token
        );
        assertNull(getPaymentRes);
    }

    @Test
    public void shouldGetPaymentStatus() throws CodifiedException {
        when(repository.getByAddress(anyString(), anyString())).thenReturn(fd.record(0));
        GetPaymentRes res = paymentQueryHandler.handle(
                new GetPaymentReq(fd.payment(0).address),
                fd.payment(0).token
        );
        assertNotNull(res);
        assertEquals(res.getAddress(), fd.payment(0).address);
        assertEquals(res.getCallbackUrl(), fd.payment(0).callbackUrl);
        assertEquals(res.getBalance(), "0");
        assertNotNull(res.getCreatedOn());
        assertEquals(res.getEvents().size(), 0);
    }

}
