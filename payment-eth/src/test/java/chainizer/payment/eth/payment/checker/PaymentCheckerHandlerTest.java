package chainizer.payment.eth.payment.checker;

import chainizer.payment.eth.payment.BaseTest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class PaymentCheckerHandlerTest extends BaseTest {

    @Mock
    private CheckRecordsTask checkRecordsTask;

    @InjectMocks
    private PaymentCheckerHandler paymentCheckerHandler;

    @Test
    public void shouldDelegate() {
        when(checkRecordsTask.call()).thenReturn(true);
        boolean result = paymentCheckerHandler.handle(new CheckPaymentsCmd());
        assertTrue(result);
    }

}
