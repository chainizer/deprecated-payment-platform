package chainizer.payment.eth.payment;

import chainizer.payment.eth.support.Fixtures;
import chainizer.payment.eth.support.Log;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.parity.Parity;
import org.web3j.utils.Convert;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class BaseTest {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    protected Fixtures fd;

    public static void sendTo(Parity parity, String address, String ether) {
        try {
            String coinbase = parity.ethCoinbase().send().getAddress();
            BigInteger balance = parity.ethGetBalance(coinbase, DefaultBlockParameterName.LATEST).send().getBalance();
            Log.info("coinbase {}", Convert.fromWei(new BigDecimal(balance), Convert.Unit.ETHER));
            parity.personalUnlockAccount(coinbase, "").send();
            parity.ethSendTransaction(new Transaction(
                    coinbase,
                    null,
                    null,
                    null,
                    address,
                    Convert.toWei(ether, Convert.Unit.ETHER).toBigInteger(),
                    null
            )).send();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void sendTo(Parity parity, String address) {
        sendTo(parity, address, "2");
    }

    @Before
    public void before() {
        fd = new Fixtures();
    }

}

