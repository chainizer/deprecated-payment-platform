package chainizer.payment.eth.payment;

import chainizer.payment.eth.support.IgnoreMessageFields;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.axonframework.test.matchers.Matchers;
import org.junit.Before;

import static org.axonframework.test.matchers.Matchers.equalTo;
import static org.axonframework.test.matchers.Matchers.payloadsMatching;

public class PaymentTest extends BaseTest {

    private FixtureConfiguration<Payment> fixture;

    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(Payment.class);
    }

    @org.junit.Test
    public void shouldPubPaymentGeneratedEvt() {
        fixture.givenNoPriorActivity()
                .when(fd.createGeneratePaymentCmd(0))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(Matchers.exactSequenceOf(
                        equalTo(fd.createPaymentGeneratedEvt(0), IgnoreMessageFields.ignoreMessageFields())
                )));
    }

    @org.junit.Test
    public void shouldPubNotificationSentEvt() {
        fixture.given(fd.createPaymentGeneratedEvt(0))
                .when(fd.createLogNotificationCmd(0, null))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(Matchers.exactSequenceOf(
                        equalTo(fd.createNotificationSucceedEvt(0), IgnoreMessageFields.ignoreMessageFields())
                )));
    }

    @org.junit.Test
    public void shouldPubNotificationFailedEvt() {
        fixture.given(fd.createPaymentGeneratedEvt(0))
                .when(fd.createLogNotificationCmd(0, "an error"))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(Matchers.exactSequenceOf(
                        equalTo(fd.createNotificationFailedEvt(0, "an error"), IgnoreMessageFields.ignoreMessageFields())
                )));
    }

    @org.junit.Test
    public void shouldPubWithdrawnSentEvt() {
        fixture.given(fd.createPaymentGeneratedEvt(0))
                .when(fd.createLogWithdrawCmd(0, "a", "m", "u", "f"))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(Matchers.exactSequenceOf(
                        equalTo(fd.createWithdrawSucceedEvt(0), IgnoreMessageFields.ignoreMessageFields())
                )));
    }

    @org.junit.Test
    public void shouldPubWithdrawnFailedEvt() {
        fixture.given(fd.createPaymentGeneratedEvt(0))
                .when(fd.createLogWithdrawCmd(0, "a", "m", "e"))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(Matchers.exactSequenceOf(
                        equalTo(fd.createWithdrawFailedEvt(0, "e"), IgnoreMessageFields.ignoreMessageFields())
                )));
    }

}
