package chainizer.payment.eth.rest;

import chainizer.payment.eth.support.Fixtures;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.container.Container;
import chainizer.support.rest.RestResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class WithdrawPaymentIntegrationTest extends JerseyTest {

    private Fixtures fd;

    @BeforeClass
    public static void beforeClass() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }

    @Before
    public void before() {
        fd = new Fixtures();
    }

    @After
    public void after() {
        Container.get().stop();
    }

    @Override
    protected Application configure() {
        return new RestResourceConfig(TestUtils.resolveConfiguration()).register(PaymentResource.class);
    }

    @Test
    public void shouldFailedWhenAddressNotFound() {
        Response response = target("/api/payment/" + fd.payment(0).address + "/withdraw")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("authorization", "Bearer token")
                .post(Entity.json(new WithdrawPaymentBodyReq(
                        fd.payment(1).address,
                        ""
                )));
        assertEquals(response.getStatus(), 404);
    }

    @Test
    public void shouldFailedWhenNoAuthorization() {
        Response response = target("/api/payment/" + fd.payment(0).address + "/withdraw")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(new WithdrawPaymentBodyReq(
                        fd.payment(1).address,
                        ""
                )));
        assertEquals(response.getStatus(), 401);
    }

    @Test
    public void shouldFailedWhenNoAuthorizationToken() {
        Response response = target("/api/payment/" + fd.payment(0).address + "/withdraw")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("authorization", "Bearer")
                .post(Entity.json(new WithdrawPaymentBodyReq(
                        fd.payment(1).address,
                        ""
                )));
        assertEquals(response.getStatus(), 401);
    }

}
