package chainizer.payment.eth.rest;

import chainizer.payment.eth.support.TestUtils;
import chainizer.support.container.Container;
import chainizer.support.rest.RestResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GeneratePaymentIntegrationTest extends JerseyTest {

    @BeforeClass
    public static void beforeClass() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }

    @After
    public void after() {
        Container.get().stop();
    }

    @Override
    protected Application configure() {
        return new RestResourceConfig(TestUtils.resolveConfiguration()).register(PaymentResource.class);
    }

    @Test
    public void shouldGeneratePayment() {
        Response response = target("/api/payment")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(new GeneratePaymentBodyReq("http://www.google.com")));
        assertEquals(response.getStatus(), 201);
        GeneratePaymentBodyRes res = response.readEntity(GeneratePaymentBodyRes.class);
        assertNotNull(res.getAddress());
        assertNotNull(res.getToken());
    }

    @Test
    public void shouldNotGeneratePaymentWhenReqNotValid() {
        Response response = target("/api/payment")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(new GeneratePaymentBodyReq("www.google.com")));
        assertEquals(response.getStatus(), 400);
    }

}
