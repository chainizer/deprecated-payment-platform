package chainizer.payment.eth.rest;

import chainizer.payment.eth.payment.BaseTest;
import chainizer.payment.eth.support.Fixtures;
import chainizer.payment.eth.support.TestUtils;
import chainizer.support.container.Container;
import chainizer.support.rest.RestResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.web3j.protocol.parity.Parity;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

public class AllInOneIntegrationTest extends JerseyTest {

    private Fixtures fd;

    @BeforeClass
    public static void beforeClass() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }

    @Before
    public void before() {
        fd = new Fixtures();
    }

    @After
    public void after() {
        Container.get().stop();
    }

    @Override
    protected Application configure() {
        return new RestResourceConfig(TestUtils.resolveConfiguration()).register(PaymentResource.class);
    }

    @Test
    public void shouldGenerateGetAndWithdraw() {
        Response generateResponse = target("/api/payment")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(new GeneratePaymentBodyReq("http://www.google.com")));
        assertEquals(generateResponse.getStatus(), 201);
        GeneratePaymentBodyRes generateBodyRes = generateResponse.readEntity(GeneratePaymentBodyRes.class);
        assertNotNull(generateBodyRes.getAddress());
        assertNotNull(generateBodyRes.getToken());

        BaseTest.sendTo(Container.get().get(Parity.class), generateBodyRes.getAddress());

        Response getResponse1 = target("/api/payment/" + generateBodyRes.getAddress())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("authorization", "Bearer " + generateBodyRes.getToken())
                .get();
        assertEquals(getResponse1.getStatus(), 200);
        PaymentBodyRes paymentBodyRes1 = getResponse1.readEntity(PaymentBodyRes.class);
        assertEquals(generateBodyRes.getAddress(), paymentBodyRes1.getAddress());
        assertEquals("http://www.google.com", paymentBodyRes1.getCallbackUrl());
        assertNotNull(paymentBodyRes1.getCreatedOn());
        assertEquals("2000000000000000000", paymentBodyRes1.getBalance());
        assertTrue(paymentBodyRes1.getEvents().isEmpty());

        Response withdrawResponse = target("/api/payment/" + generateBodyRes.getAddress() + "/withdraw")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("authorization", "Bearer " + generateBodyRes.getToken())
                .post(Entity.json(new WithdrawPaymentBodyReq(
                        fd.payment(0).address,
                        ""
                )));
        assertEquals(withdrawResponse.getStatus(), 201);
        WithdrawPaymentBodyRes withdrawBodyResponse = withdrawResponse.readEntity(WithdrawPaymentBodyRes.class);
        assertNotNull(withdrawBodyResponse.getHash());

        Response getResponse2 = target("/api/payment/" + generateBodyRes.getAddress())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("authorization", "Bearer " + generateBodyRes.getToken())
                .get();
        assertEquals(getResponse2.getStatus(), 200);
        PaymentBodyRes paymentBodyRes2 = getResponse2.readEntity(PaymentBodyRes.class);
        assertFalse(paymentBodyRes2.getEvents().isEmpty());
        assertEquals("withdraw", paymentBodyRes2.getEvents().get(0).getType());
        assertNotNull(paymentBodyRes2.getEvents().get(0).getAttemptedOn());
        assertNull(paymentBodyRes2.getEvents().get(0).getError());
    }

}
