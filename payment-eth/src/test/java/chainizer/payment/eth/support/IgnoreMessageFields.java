package chainizer.payment.eth.support;

import org.axonframework.test.matchers.FieldFilter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class IgnoreMessageFields implements FieldFilter {

    private final List<String> names;

    private IgnoreMessageFields(List<String> names) {
        this.names = names;
    }

    public static IgnoreMessageFields ignoreMessageFields() {
        return new IgnoreMessageFields(Arrays.asList("createdOn", "succeedOn", "failedOn"));
    }

    @Override
    public boolean accept(Field field) {
        return !names.contains(field.getName());
    }

}