package chainizer.payment.eth.support;

import chainizer.payment.eth.payment.*;
import chainizer.payment.eth.payment.repository.Record;
import chainizer.payment.eth.payment.withdraw.WithdrawPaymentCmd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.utils.Numeric;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Fixtures {

    private final static Logger LOGGER = LoggerFactory.getLogger(Fixtures.class);

    public final List<Payment> PAYMENTS = new ArrayList<>();

    public Payment payment(int index) {
        if (PAYMENTS.size() > index) {
            return PAYMENTS.get(index);
        }
        for (int i = PAYMENTS.size(); i < index + 1; i++) {
            PAYMENTS.add(new Payment());
        }
        return PAYMENTS.get(index);
    }

    public Record record(int index) {
        Fixtures.Payment p = payment(index);
        return new Record(
                p.address,
                p.encryptedPrivateKey,
                p.challenge,
                p.callbackUrl,
                p.createdOn,
                p.notified,
                p.lastNotificationAttemptedOn,
                p.withdrawn,
                Collections.emptyList()
        );
    }

    public GeneratePaymentCmd createGeneratePaymentCmd(int index) {
        Payment payment = payment(index);
        return new GeneratePaymentCmd(
                payment.address,
                payment.encryptedPrivateKey,
                payment.challenge,
                payment.callbackUrl
        );
    }

    public PaymentGeneratedEvt createPaymentGeneratedEvt(int index) {
        Payment payment = payment(index);
        return new PaymentGeneratedEvt(
                payment.address,
                payment.encryptedPrivateKey,
                payment.challenge,
                payment.callbackUrl,
                payment.createdOn
        );
    }

    public LogCallResultCmd createLogNotificationCmd(int index, String error) {
        return new LogCallResultCmd(
                payment(index).address,
                error
        );
    }

    public LogWithdrawCmd createLogWithdrawCmd(int index, String toAddress, String message, String error) {
        return new LogWithdrawCmd(
                payment(index).address,
                toAddress,
                message,
                error
        );
    }

    public LogWithdrawCmd createLogWithdrawCmd(int index, String toAddress, String message, String feeHash, String userHash) {
        return new LogWithdrawCmd(
                payment(index).address,
                toAddress,
                message,
                feeHash,
                userHash
        );
    }

    public NotificationSucceedEvt createNotificationSucceedEvt(int index) {
        return new NotificationSucceedEvt(
                payment(index).address,
                Instant.now()
        );
    }

    public NotificationFailedEvt createNotificationFailedEvt(int index, String error) {
        return new NotificationFailedEvt(
                payment(index).address,
                Instant.now(),
                error
        );
    }

    public WithdrawSucceedEvt createWithdrawSucceedEvt(int index) {
        return new WithdrawSucceedEvt(
                payment(index).address,
                Instant.now()
        );
    }

    public WithdrawFailedEvt createWithdrawFailedEvt(int index, String error) {
        return new WithdrawFailedEvt(
                payment(index).address,
                Instant.now(),
                error
        );
    }

    public WithdrawPaymentCmd createWithdrawPaymentCmd(int from, int to) {
        return new WithdrawPaymentCmd(
                payment(from).address,
                payment(to).address,
                "paymentId"
        );
    }

    public static class Payment {
        public final String token = UUID.randomUUID().toString();
        public String address;
        public String encryptedPrivateKey;
        public String challenge;
        public String callbackUrl;
        public Instant createdOn;
        public boolean notified;
        public Instant lastNotificationAttemptedOn;
        public boolean withdrawn;

        public Payment() {
            try {
                ECKeyPair ecKeyPair = Keys.createEcKeyPair();
                address = Numeric.prependHexPrefix(Keys.getAddress(ecKeyPair));
                String privateKey = Numeric.toHexStringWithPrefix(ecKeyPair.getPrivateKey());
                encryptedPrivateKey = Crypto.encrypt(privateKey, token);
                challenge = Crypto.toChallenge(token);
                callbackUrl = "http://google.com";
                createdOn = Instant.now().minus(15, ChronoUnit.MINUTES);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
