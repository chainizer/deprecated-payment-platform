package chainizer.payment.eth.support;

import chainizer.support.core.Configurations;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class TestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);

    public static String environment() {
        return isNotBlank(System.getenv("CI")) ? "ci" : "local";
    }

    public static Configuration resolveConfiguration() {
        Configuration configuration;

        if (isNotBlank(System.getenv("CI"))) {
            configuration = Configurations.ci(TestUtils.class.getResource("/payment-eth.properties"));
        } else {
            configuration = Configurations.local(TestUtils.class.getResource("/payment-eth.properties"));
        }

        return configuration;
    }

    public static Parity createParity(Configuration configuration) {
        return Parity.build(
                new HttpService(
                        configuration.getString("chainizer.payment.eth.parity.endpoint")
                )
        );
    }
}
