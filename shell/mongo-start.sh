#!/usr/bin/env bash

if [ -z ${CI} ]; then
    docker stop platform-mongo
    docker run \
        --rm $1 \
        --publish 27017:27017 \
        --name platform-mongo \
        mongo:3.6
fi
