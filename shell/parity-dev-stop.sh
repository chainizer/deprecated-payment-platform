#!/usr/bin/env bash

if [ -z ${CI} ]; then
    docker stop payment-eth-parity-dev
fi
