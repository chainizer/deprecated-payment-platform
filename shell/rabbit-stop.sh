#!/usr/bin/env bash

if [ -z ${CI} ]; then
    docker stop platform-rabbit
fi
