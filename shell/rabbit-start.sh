#!/usr/bin/env bash

if [ -z ${CI} ]; then
    docker stop platform-rabbit
    docker run \
        --name platform-rabbit \
        -p 5672:5672 \
        -d \
        rabbitmq:3-alpine
fi

sleep 2
