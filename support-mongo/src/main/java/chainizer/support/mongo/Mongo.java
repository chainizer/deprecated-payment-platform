package chainizer.support.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utilities around MongoDB.
 */
public final class Mongo {

    private final static Logger LOGGER = LoggerFactory.getLogger(Mongo.class);

    private final static Map<String, MongoClient> CLIENTS = new ConcurrentHashMap<>();

    public static void close() {
        LOGGER.info("close {} clients", CLIENTS.size());
        CLIENTS.values().forEach(com.mongodb.Mongo::close);
        CLIENTS.clear();
    }

    /**
     * Get a client from the given configuration
     *
     * @param configuration the configuration
     * @return the client
     */
    public static MongoClient getClient(Configuration configuration) {
        String uri = configuration.getString("mongodb.uri");
        if (!CLIENTS.containsKey(uri)) {
            String database = configuration.getString("mongodb.database");
            boolean drop = configuration.getBoolean("mongodb.drop_database", false);
            LOGGER.info("configuration: {} - {} - {}", uri, database, drop);

            MongoClient mongoClient = new MongoClient(new MongoClientURI(uri));
            CLIENTS.put(uri, mongoClient);

            if (drop) {
                LOGGER.info("dropping: {} - {}", uri, database);
                mongoClient.getDatabase(database).drop();
            }
        }
        return CLIENTS.get(uri);
    }

}
