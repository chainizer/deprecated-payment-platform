package chainizer.support.container;

import chainizer.support.core.CodifiedMessage;

public enum ContainerErrorMsg implements CodifiedMessage {
    CONTAINER_NOT_CONFIGURED(500, "The container is not configured.");

    private final int code;

    private final String template;

    ContainerErrorMsg(int code, String template) {
        this.template = template;
        this.code = code;
    }

    @Override
    public String format(Object... params) {
        return String.format(template, params);
    }

    @Override
    public int code() {
        return code;
    }

}

