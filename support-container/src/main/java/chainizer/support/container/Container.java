package chainizer.support.container;

import chainizer.support.axon.AxonModule;
import chainizer.support.core.TechnicalException;
import org.axonframework.config.Configuration;
import org.axonframework.config.Configurer;
import org.axonframework.config.DefaultConfigurer;
import org.glassfish.hk2.utilities.Binder;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

import static chainizer.support.container.ContainerErrorMsg.CONTAINER_NOT_CONFIGURED;

public class Container {

    private final static Logger LOGGER = LoggerFactory.getLogger(Container.class);

    private static Container INSTANCE = new Container();
    private final AtomicBoolean configured = new AtomicBoolean(false);
    private Configuration axonConfiguration;
    private Function<Class<?>, Object> get;

    public Container() {
    }

    public static Container get() {
        if (INSTANCE == null) {
            INSTANCE = new Container();
        }
        return INSTANCE;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> type) {
        if (!configured.get()) {
            throw new TechnicalException(CONTAINER_NOT_CONFIGURED);
        }
        return (T) this.get.apply(type);
    }

    public Container configure(
            Function<Class<?>, Object> get,
            Consumer<Object> inject,
            Consumer<Binder> register,
            Binder... resources
    ) {
        LOGGER.info("configure container");

        this.get = get;

        Configurer axonConfigurer = (Configurer) get.apply(Configurer.class);
        if (axonConfigurer == null) {
            LOGGER.info("No Axon Configurer found: use the default one!");
            axonConfigurer = DefaultConfigurer.defaultConfiguration();
        }

        axonConfigurer.configureResourceInjector(c -> inject::accept);

        ServiceLoader<Binder> loadedBinders = ServiceLoader.load(Binder.class);
        for (Binder binder : loadedBinders) {
            LOGGER.info("load hk2 binder {}", binder);
            register.accept(binder);
        }

        for (Binder resource : resources) {
            LOGGER.info("load hk2 binder {}", resource);
            register.accept(resource);
        }

        ServiceLoader<AxonModule> loadedAxonModules = ServiceLoader.load(AxonModule.class);
        for (AxonModule axonModule : loadedAxonModules) {
            LOGGER.info("load Axon module {}", axonModule);
            inject.accept(axonModule);
            axonModule.configure(axonConfigurer);
        }

        axonConfiguration = axonConfigurer.buildConfiguration();
        register.accept(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(axonConfiguration).to(Configuration.class);
            }
        });

        configured.set(true);

        return this;
    }

    public Container start() {
        LOGGER.info("start container");
        axonConfiguration.start();
        return this;
    }

    public Container stop() {
        LOGGER.info("stop container");
        try {
            axonConfiguration.shutdown();
        } catch (Exception e) {
            LOGGER.warn("unable to properly stop the container", e);
        }
        return this;
    }

}
