package chainizer.support.axon;

import org.axonframework.config.Configurer;

public interface AxonModule {

    void configure(Configurer configurer);

}
