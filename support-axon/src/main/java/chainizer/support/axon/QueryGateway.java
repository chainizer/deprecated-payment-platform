package chainizer.support.axon;

import org.axonframework.queryhandling.QueryMessage;

public interface QueryGateway {

    @SuppressWarnings("unused")
    <T, R> R sendAndWait(QueryMessage<T, R> query);

    @SuppressWarnings("unused")
    <T, R> R sendAndWait(T req, Class<R> responseType, String token);

}
