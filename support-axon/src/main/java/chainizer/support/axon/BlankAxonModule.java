package chainizer.support.axon;

import org.axonframework.config.ModuleConfiguration;

public interface BlankAxonModule extends ModuleConfiguration {

    @Override
    default void start() {
    }

    @Override
    default void shutdown() {
    }

}
