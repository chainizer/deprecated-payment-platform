package chainizer.support.axon;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.messaging.interceptors.BeanValidationInterceptor;
import org.glassfish.hk2.api.Factory;

import javax.inject.Inject;

public class CommandGatewayFactory implements Factory<CommandGateway> {

    private final CommandBus commandBus;

    @Inject
    public CommandGatewayFactory(CommandBus commandBus) {
        this.commandBus = commandBus;
    }

    @Override
    public CommandGateway provide() {
        org.axonframework.commandhandling.gateway.CommandGatewayFactory factory = new org.axonframework.commandhandling.gateway.CommandGatewayFactory(commandBus);
        factory.registerDispatchInterceptor(new BeanValidationInterceptor<>());
        factory.registerCommandCallback(LoggingAxonCallback.INSTANCE);
        return factory.createGateway(CommandGateway.class);
    }

    @Override
    public void dispose(CommandGateway instance) {
    }

}
