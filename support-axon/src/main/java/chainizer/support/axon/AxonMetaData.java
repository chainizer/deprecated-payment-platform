package chainizer.support.axon;

import java.util.HashMap;
import java.util.Map;

public class AxonMetaData {

    private final Map<String, Object> metaData = new HashMap<>();

    private AxonMetaData() {
    }

    public static AxonMetaData empty() {
        return new AxonMetaData();
    }

    public AxonMetaData put(String key, Object value) {
        this.metaData.put(key, value);
        return this;
    }

    public Map<String, ?> get() {
        return this.metaData;
    }
}
