package chainizer.support.axon;

import chainizer.support.core.TechnicalException;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.axonframework.queryhandling.GenericQueryMessage;
import org.axonframework.queryhandling.QueryBus;
import org.axonframework.queryhandling.QueryMessage;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class QueryGatewayImpl implements QueryGateway {

    private final QueryBus queryBus;
    private final MessageDispatchInterceptor<? super QueryMessage<?, ?>>[] dispatchInterceptors;

    /**
     * Initializes the gateway to sendAndWait queries to the given {@code queryBus} and invoking given
     * {@code dispatchInterceptors} prior to publication ont he query bus.
     *
     * @param queryBus             The bus to deliver messages on
     * @param dispatchInterceptors The interceptors to invoke prior to publication on the bus
     */
    @SafeVarargs
    public QueryGatewayImpl(QueryBus queryBus, MessageDispatchInterceptor<? super QueryMessage<?, ?>>... dispatchInterceptors) {
        this.queryBus = queryBus;
        this.dispatchInterceptors = dispatchInterceptors;
    }

    @Override
    public <T, R> R sendAndWait(QueryMessage<T, R> query) {
        try {
            return queryBus.query(query).get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public <T, R> R sendAndWait(T req, Class<R> responseType, String token) {
        return sendAndWait(
                new GenericQueryMessage<>(req, responseType)
                        .andMetaData(AxonMetaData.empty().put("token", token).get())
        );
    }

}
