package chainizer.support.axon;

import chainizer.support.mongo.Mongo;
import org.apache.commons.configuration2.Configuration;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.config.Configurer;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.mongo.DefaultMongoTemplate;
import org.axonframework.mongo.eventsourcing.eventstore.MongoEventStorageEngine;
import org.axonframework.queryhandling.QueryBus;

import javax.inject.Inject;

public class MongoDbAxonModule implements AxonModule {

    @Inject
    private CommandBus commandBus;

    @Inject
    private EventBus eventBus;

    @Inject
    private QueryBus queryBus;

    @Inject
    private Configuration configuration;

    @Override
    public void configure(Configurer configurer) {
        configurer.configureCommandBus(configuration -> commandBus);

        configurer.configureEventBus(configuration -> eventBus);

        configurer.configureQueryBus(configuration -> queryBus);

        configurer.configureEmbeddedEventStore(c -> new MongoEventStorageEngine(
                new DefaultMongoTemplate(
                        Mongo.getClient(configuration.subset("chainizer.payment.eth.axon")),
                        configuration.getString("chainizer.payment.eth.axon.mongodb.database")
                )
        ));
    }

}
