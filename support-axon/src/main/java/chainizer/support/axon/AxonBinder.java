package chainizer.support.axon;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.common.transaction.NoTransactionManager;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.SimpleEventBus;
import org.axonframework.messaging.correlation.MessageOriginProvider;
import org.axonframework.messaging.interceptors.CorrelationDataInterceptor;
import org.axonframework.monitoring.NoOpMessageMonitor;
import org.axonframework.queryhandling.QueryBus;
import org.axonframework.queryhandling.SimpleQueryBus;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import java.util.Collections;

public class AxonBinder extends AbstractBinder {

    @Override
    protected void configure() {
        SimpleCommandBus simpleCommandBus = new SimpleCommandBus(
                NoTransactionManager.INSTANCE,
                NoOpMessageMonitor.instance()
        );
        simpleCommandBus.registerHandlerInterceptor(
                new CorrelationDataInterceptor<>(Collections.singletonList(new MessageOriginProvider()))
        );
        bind(simpleCommandBus).to(CommandBus.class);

        SimpleEventBus simpleEventBus = new SimpleEventBus(
                Integer.MAX_VALUE,
                NoOpMessageMonitor.instance()
        );
        bind(simpleEventBus).to(EventBus.class);

        SimpleQueryBus simpleQueryBus = new SimpleQueryBus(
                NoTransactionManager.INSTANCE
        );
        bind(simpleQueryBus).to(QueryBus.class);

        bindFactory(CommandGatewayFactory.class).to(CommandGateway.class);

        bindFactory(QueryGatewayFactory.class).to(QueryGateway.class);
    }

}
