package chainizer.support.axon;

import org.axonframework.commandhandling.CommandCallback;
import org.axonframework.commandhandling.CommandMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingAxonCallback implements CommandCallback<Object, Object> {


    /**
     * The singleton instance for this callback
     */
    public static final LoggingAxonCallback INSTANCE = new LoggingAxonCallback();
    private static final Logger logger = LoggerFactory.getLogger(LoggingAxonCallback.class);

    private LoggingAxonCallback() {
    }

    @Override
    public void onSuccess(CommandMessage message, Object result) {
        logger.info(
                "{}",
                logger.isDebugEnabled() ? message.getPayload() : message.getPayloadType().getName()
        );
    }

    @Override
    public void onFailure(CommandMessage<?> message, Throwable cause) {
        logger.warn(
                String.format(
                        "%s|%s",
                        message.getPayloadType().getName(),
                        message.getPayload()
                ),
                cause
        );
    }

}
