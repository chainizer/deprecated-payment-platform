package chainizer.support.axon;

import org.axonframework.config.Configurer;
import org.axonframework.config.EventHandlingConfiguration;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.EventMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventLoggerAxonModule implements AxonModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventLoggerAxonModule.class);

    @Override
    public void configure(Configurer configurer) {
        configurer.registerModule(
                new EventHandlingConfiguration().registerEventHandler(configuration -> new Object() {
                    @EventHandler
                    public void listen(EventMessage eventMessage) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug(
                                    "{}|{}",
                                    eventMessage.getPayloadType().getName(),
                                    eventMessage.getPayload()
                            );
                        } else {
                            LOGGER.info(
                                    "{}",
                                    eventMessage.getPayloadType().getName()
                            );
                        }
                    }
                })
        );
    }

}
