package chainizer.support.axon;

import org.axonframework.messaging.interceptors.BeanValidationInterceptor;
import org.axonframework.queryhandling.QueryBus;
import org.glassfish.hk2.api.Factory;

import javax.inject.Inject;

public class QueryGatewayFactory implements Factory<QueryGateway> {

    private final QueryBus queryBus;

    @Inject
    public QueryGatewayFactory(QueryBus queryBus) {
        this.queryBus = queryBus;
    }

    @Override
    public QueryGateway provide() {
        return new QueryGatewayImpl(
                queryBus,
                new BeanValidationInterceptor<>()
        );
    }

    @Override
    public void dispose(QueryGateway instance) {
    }

}
