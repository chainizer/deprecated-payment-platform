package chainizer.support.axon;

import org.axonframework.commandhandling.gateway.Timeout;
import org.axonframework.messaging.annotation.MetaDataValue;

import java.util.concurrent.TimeUnit;

public interface CommandGateway {

    @Timeout(value = 1, unit = TimeUnit.SECONDS)
    @SuppressWarnings("unused")
    <T, R> R sendAndWait(T req);

    @Timeout(value = 1, unit = TimeUnit.SECONDS)
    @SuppressWarnings("unused")
    <T, R> R sendAndWait(T req, @MetaDataValue("token") String token);

    @SuppressWarnings("unused")
    <T> void send(T command);

}
